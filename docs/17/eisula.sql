/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.31 : Database - eisula
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`eisula` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `eisula`;

/*Table structure for table `advance` */

DROP TABLE IF EXISTS `advance`;

CREATE TABLE `advance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date_present` date NOT NULL,
  `porcentage` varchar(30) NOT NULL,
  `proyect_id` int(10) NOT NULL,
  `file` varchar(255) NOT NULL,
  `acta` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `avance_proyect` (`proyect_id`),
  KEY `advance_id` (`id`),
  KEY `advance_proyect` (`proyect_id`),
  CONSTRAINT `advance_proyect` FOREIGN KEY (`proyect_id`) REFERENCES `proyect` (`id`),
  CONSTRAINT `avance_proyect` FOREIGN KEY (`proyect_id`) REFERENCES `proyect` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `advance` */

/*Table structure for table `document` */

DROP TABLE IF EXISTS `document`;

CREATE TABLE `document` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `version` tinyint(4) NOT NULL,
  `date_preset` date NOT NULL,
  `acta_final` varchar(255) NOT NULL,
  `proyect_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `document_proyect` (`proyect_id`),
  KEY `document_id` (`id`),
  CONSTRAINT `document_proyect` FOREIGN KEY (`proyect_id`) REFERENCES `proyect` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `document` */

/*Table structure for table `proyect` */

DROP TABLE IF EXISTS `proyect`;

CREATE TABLE `proyect` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `priority` varchar(20) DEFAULT NULL,
  `date_present` date DEFAULT NULL,
  `semester` varchar(10) NOT NULL,
  `department` char(1) DEFAULT NULL,
  `status` char(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `agency` varchar(20) DEFAULT NULL,
  `tutor` int(10) NOT NULL,
  `cotutor` int(10) DEFAULT NULL,
  `jury1` int(10) DEFAULT NULL,
  `jury2` int(10) DEFAULT NULL,
  `jury3` int(10) DEFAULT NULL,
  `date_present_advance` date DEFAULT NULL,
  `proposal` varchar(255) DEFAULT NULL,
  `porcentage` varchar(30) DEFAULT NULL,
  `observations` varchar(255) DEFAULT NULL,
  `score_final` int(10) DEFAULT NULL,
  `manuscript` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `proyect_student` (`student_id`),
  KEY `proyect_id` (`id`),
  KEY `proyect_teacher` (`tutor`),
  KEY `proyect_teacher2` (`cotutor`),
  KEY `proyect_teacher3` (`jury1`),
  KEY `proyect_teacher4` (`jury2`),
  KEY `proyect_teacher5` (`jury3`),
  CONSTRAINT `proyect_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `proyect_teacher` FOREIGN KEY (`tutor`) REFERENCES `teacher` (`id`),
  CONSTRAINT `proyect_teacher2` FOREIGN KEY (`cotutor`) REFERENCES `teacher` (`id`),
  CONSTRAINT `proyect_teacher3` FOREIGN KEY (`jury1`) REFERENCES `teacher` (`id`),
  CONSTRAINT `proyect_teacher4` FOREIGN KEY (`jury2`) REFERENCES `teacher` (`id`),
  CONSTRAINT `proyect_teacher5` FOREIGN KEY (`jury3`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `proyect` */

insert  into `proyect`(`id`,`title`,`priority`,`date_present`,`semester`,`department`,`status`,`student_id`,`agency`,`tutor`,`cotutor`,`jury1`,`jury2`,`jury3`,`date_present_advance`,`proposal`,`porcentage`,`observations`,`score_final`,`manuscript`) values (42,'prueba','','2015-08-01','A-2015','3','12',8,NULL,6,NULL,6,13,NULL,'2015-07-02','/docs/8/transferencia seguro.pdf','85','545',19,NULL),(43,'PRUEBA CARRTA','','2015-08-10','A-2015','3','2',1,NULL,3,NULL,3,6,NULL,'2015-06-30','/docs/1/.gitignore','41','Prueba de observaciones, en avance',16,NULL),(49,'hola','mundo',NULL,'A-2015','3','10',15,NULL,13,NULL,6,6,13,'2015-08-11','/docs/15/constancia.pdf','15','15 %',NULL,NULL),(50,'Nueva prueba','nada',NULL,'A-2015','3','1',16,NULL,6,6,6,13,3,'2015-07-08','/docs/16/Notify.php','80','hola',19,'/docs/16/renta mama julio.pdf'),(52,'Prueba 2','materia de prueba 2','2015-08-10','A-2015','3','13',17,NULL,13,3,3,6,13,'2015-06-04','/docs/17/constancia.pdf','60','avance de prueba 2',18,'/docs/17/renta yo julio.pdf'),(53,'prueba de rechazo','prueba',NULL,'A-2015',NULL,'1',1,NULL,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `review` */

DROP TABLE IF EXISTS `review`;

CREATE TABLE `review` (
  `document_id` int(10) NOT NULL,
  `teacher_id` int(10) NOT NULL,
  `revised` tinyint(4) NOT NULL,
  `date_review` date NOT NULL,
  PRIMARY KEY (`document_id`,`teacher_id`),
  KEY `review_document` (`document_id`),
  KEY `review_teacher` (`teacher_id`),
  CONSTRAINT `review_document` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`),
  CONSTRAINT `review_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `review` */

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `student_user` (`id`),
  CONSTRAINT `student_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`id`) values (1),(2),(8),(15),(16),(17);

/*Table structure for table `system_setting` */

DROP TABLE IF EXISTS `system_setting`;

CREATE TABLE `system_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jefe_depto_control` int(10) DEFAULT NULL,
  `jefe_depto_computacion` int(10) DEFAULT NULL,
  `jefe_depto_investigacion` int(10) DEFAULT NULL,
  `secrt_depto_control` int(10) DEFAULT NULL,
  `secrt_depto_investigacion` int(10) DEFAULT NULL,
  `secrt_depto_computacion` int(10) DEFAULT NULL,
  `semester` varchar(10) NOT NULL,
  `next_semester` varchar(10) DEFAULT NULL,
  `chief_depto_control` int(10) DEFAULT NULL,
  `chief_depto_computacion` int(10) DEFAULT NULL,
  `chief_depto_investigacion` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `system_setting_user` (`secrt_depto_computacion`),
  KEY `system_setting_user2` (`secrt_depto_control`),
  KEY `system_setting_user3` (`secrt_depto_investigacion`),
  KEY `system_setting_user4` (`jefe_depto_computacion`),
  KEY `system_setting_user5` (`jefe_depto_investigacion`),
  KEY `system_setting_user6` (`jefe_depto_control`),
  KEY `system_setting_id` (`id`),
  CONSTRAINT `system_setting_user` FOREIGN KEY (`secrt_depto_computacion`) REFERENCES `user` (`id`),
  CONSTRAINT `system_setting_user2` FOREIGN KEY (`secrt_depto_control`) REFERENCES `user` (`id`),
  CONSTRAINT `system_setting_user3` FOREIGN KEY (`secrt_depto_investigacion`) REFERENCES `user` (`id`),
  CONSTRAINT `system_setting_user4` FOREIGN KEY (`jefe_depto_computacion`) REFERENCES `user` (`id`),
  CONSTRAINT `system_setting_user5` FOREIGN KEY (`jefe_depto_investigacion`) REFERENCES `user` (`id`),
  CONSTRAINT `system_setting_user6` FOREIGN KEY (`jefe_depto_control`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `system_setting` */

insert  into `system_setting`(`id`,`jefe_depto_control`,`jefe_depto_computacion`,`jefe_depto_investigacion`,`secrt_depto_control`,`secrt_depto_investigacion`,`secrt_depto_computacion`,`semester`,`next_semester`,`chief_depto_control`,`chief_depto_computacion`,`chief_depto_investigacion`) values (1,13,3,6,9,10,11,'A-2015','B-2015',NULL,NULL,NULL);

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `curriculum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_user` (`id`),
  CONSTRAINT `teacher_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `teacher` */

insert  into `teacher`(`id`,`curriculum`) values (3,NULL),(6,NULL),(13,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dni` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `department` char(1) NOT NULL,
  `type` char(1) NOT NULL,
  `token` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dni` (`dni`),
  KEY `id` (`id`),
  KEY `user_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`dni`,`name`,`last_name`,`email`,`password`,`department`,`type`,`token`) values (1,'15825213','Fernando','Linares','fjlv84@hotmail.com','202cb962ac59075b964b07152d234b70','2','1',''),(2,'admin','administrador','administrador','ad@ad.com','202cb962ac59075b964b07152d234b70','4','0',NULL),(3,'8003398','Dulce','Rivero','dulcem.rivero@gmail.com','202cb962ac59075b964b07152d234b70','3','2',NULL),(6,'123456','Pedro','Pérez','fjlv84@gmail.com','202cb962ac59075b964b07152d234b70','2','2',NULL),(7,'654321','Jose ','Jose','ab@ab.com','202cb962ac59075b964b07152d234b70','1','2',NULL),(8,'12345','jose ','perez','jp@jp.com','202cb962ac59075b964b07152d234b70','3','1',NULL),(9,'1','Adm.','Control','1@s.com','202cb962ac59075b964b07152d234b70','1','3',NULL),(10,'2','Adm.','Investigación','2@a.com','202cb962ac59075b964b07152d234b70','2','3',NULL),(11,'3','Adm.','Computacion','3@a.com','202cb962ac59075b964b07152d234b70','3','3',NULL),(13,'17029169','Yandri','Vergara','fjlv84@hotmail.com','202cb962ac59075b964b07152d234b70','1','2',NULL),(14,'4','Adm.','Dirección','4@4.com','202cb962ac59075b964b07152d234b70','4','0',NULL),(15,'1234567','estudiante ','prueba','ep@ep.com','202cb962ac59075b964b07152d234b70','2','1',NULL),(16,'11','aa','bb','aa@bb.com','202cb962ac59075b964b07152d234b70','1','1',NULL),(17,'22','bb','cc','bb@cc.com','202cb962ac59075b964b07152d234b70','2','1',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
