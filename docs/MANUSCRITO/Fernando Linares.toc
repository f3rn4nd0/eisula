\select@language {spanish}
\contentsline {chapter}{�ndice de Tablas}{\es@scroman {vii}}{chapter*.1}
\contentsline {chapter}{�ndice de Figuras}{\es@scroman {viii}}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Antecedentes}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Planteamiento del Problema}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Justificaci\'on}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Objetivos}{3}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Objetivos General}{3}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Objetivos Espec\IeC {\'\i }ficos}{4}{subsection.1.4.2}
\contentsline {section}{\numberline {1.5}Metodolog\IeC {\'\i }a}{4}{section.1.5}
\contentsline {section}{\numberline {1.6}Estructura del Documento}{4}{section.1.6}
\contentsline {chapter}{\numberline {2}Marco Te\'orico}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}\textit {Scrum}}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Roles del \textit {Scrum} }{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Artefactos del \textit {Scrum}}{7}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Eventos del \textit {Scrum}}{8}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Arquitectura de Dos Niveles}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Patr\'on de Dise\~no MVC}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Modelado de Negocios}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Diagrama Mapa de Procesos}{10}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Diagrama de Proceso de Negocios}{10}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Modelo de Actividades}{11}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Lenguaje Unificado de Modelado (\textit {UML})}{11}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Diagrama de Casos de Uso}{11}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Diagrama de Clases}{11}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Servidor Web (Apache HTTP)}{12}{section.2.6}
\contentsline {section}{\numberline {2.7}Gestor de Base de Datos (MySql)}{12}{section.2.7}
\contentsline {section}{\numberline {2.8}Lenguaje de Programaci\'on (PHP)}{12}{section.2.8}
\contentsline {section}{\numberline {2.9}Framework Yii}{13}{section.2.9}
\contentsline {section}{\numberline {2.10}Framework Bootstrap}{13}{section.2.10}
\contentsline {chapter}{\numberline {3}Modelado de Dominio y Especificaci\'on de Requerimientos}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Modelado de Negocios}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Cadena de Valor}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Jerarqu\IeC {\'\i }a de Procesos}{15}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Diagramas de Actividades}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Registro de Carta de Compromiso}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Registro de Propuesta}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Aprobaci\'on de Propuesta}{21}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Presentaci\'on del Avance}{21}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Solicitar Pr\'orroga}{22}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Presentaci\'on Final de Proyecto de Grado}{22}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}Modelo de Actores}{23}{section.3.3}
\contentsline {section}{\numberline {3.4}Casos de Uso}{23}{section.3.4}
\contentsline {section}{\numberline {3.5}Diagramas de Casos de Uso}{26}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Casos de Uso comunes a los Usuarios}{26}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Casos de Uso Profesor Tutor}{27}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Casos de Uso General}{27}{subsection.3.5.3}
\contentsline {chapter}{\numberline {4}Dise\~no}{28}{chapter.4}
\contentsline {section}{\numberline {4.1} \textit { Sprint} 1}{28}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Diagrama de Clases}{31}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Modelo Entidad Relaci\'on}{31}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}\textit {Sprint }2}{33}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Validaci\'on de datos de entrada}{33}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Componentes del \textit {login} }{34}{subsection.4.2.2}
\contentsline {chapter}{\numberline {5}Pruebas}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}Vistas del Sistema}{38}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Inicio de Sesi\'on}{38}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Recuperar contrase\~na}{38}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Registro de Usuarios}{39}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Registro de Estudiantes}{39}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Registro de Profesores}{40}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Registro de Personal Administrativo}{41}{subsection.5.1.6}
\contentsline {section}{\numberline {5.2}Edici\'on de Perfil}{42}{section.5.2}
\contentsline {section}{\numberline {5.3}Configurar Actores y Par\'ametros del Sistema}{43}{section.5.3}
\contentsline {section}{\numberline {5.4}Registro de Carta de Compromiso}{43}{section.5.4}
\contentsline {section}{\numberline {5.5}Registro de la Propuesta}{44}{section.5.5}
\contentsline {section}{\numberline {5.6}Discusi\'on de la Propuesta en el Departamento}{45}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Vista de Lista de Proyectos Inscritos en el Departamento}{45}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Vista de Detalles del Proyecto en Discusi\'on}{46}{subsection.5.6.2}
\contentsline {subsection}{\numberline {5.6.3}Vista de Correcci\'on de Propuesta del Proyecto en Discusi\'on en el Departamento.}{47}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}Vista de asignaci\'on de los Jurados.}{48}{subsection.5.6.4}
\contentsline {section}{\numberline {5.7}Discusi\'on de la Propuesta en el Direcci\'on.}{49}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Vista de Lista de Proyectos Inscritos en la Direcci\'on de la Escuela}{49}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}Vista de Detalles del Proyecto en Discusi\'on}{49}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Vista de Correcci\'on de Propuesta del Proyecto en Discusi\'on en la Direcci\'on.}{50}{subsection.5.7.3}
\contentsline {section}{\numberline {5.8}Registro del Avance}{51}{section.5.8}
\contentsline {section}{\numberline {5.9}Vista Solicitud del Pr\'orroga }{52}{section.5.9}
\contentsline {section}{\numberline {5.10}Registro de Acta de Presentaci\'on de Proyecto de Grado}{53}{section.5.10}
\contentsline {section}{\numberline {5.11}Carga del Documento Final}{54}{section.5.11}
\contentsline {chapter}{\numberline {6}Conclusiones y Recomendaciones}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusiones}{55}{section.6.1}
\contentsline {section}{\numberline {6.2}Recomendaciones}{56}{section.6.2}
\contentsline {chapter}{\numberline {7}Anexos}{57}{chapter.7}
\contentsline {section}{\numberline {7.1}Gr\'aficos de Avance \textit {(Sprint's)} }{57}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Sprint 1}{57}{subsection.7.1.1}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{57}{section*.4}
\contentsline {subsubsection}{Gr\'afico Individual}{58}{section*.5}
\contentsline {subsubsection}{Gr\'afico de Tareas}{58}{section*.6}
\contentsline {subsection}{\numberline {7.1.2}Sprint 2}{59}{subsection.7.1.2}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{59}{section*.7}
\contentsline {subsubsection}{Gr\'afico Individual}{59}{section*.8}
\contentsline {subsubsection}{Gr\'afico de Tareas}{60}{section*.9}
\contentsline {subsection}{\numberline {7.1.3}Sprint 3}{60}{subsection.7.1.3}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{60}{section*.10}
\contentsline {subsubsection}{Gr\'afico Individual}{61}{section*.11}
\contentsline {subsubsection}{Gr\'afico de Tareas}{61}{section*.12}
\contentsline {subsection}{\numberline {7.1.4}Sprint 4}{62}{subsection.7.1.4}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{62}{section*.13}
\contentsline {subsubsection}{Gr\'afico Individual}{62}{section*.14}
\contentsline {subsubsection}{Gr\'afico de Tareas}{63}{section*.15}
\contentsline {subsection}{\numberline {7.1.5}Sprint 5}{63}{subsection.7.1.5}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{63}{section*.16}
\contentsline {subsubsection}{Gr\'afico Individual}{64}{section*.17}
\contentsline {subsubsection}{Gr\'afico de Tareas}{64}{section*.18}
\contentsline {subsection}{\numberline {7.1.6}Sprint 6}{65}{subsection.7.1.6}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{65}{section*.19}
\contentsline {subsubsection}{Gr\'afico Individual}{65}{section*.20}
\contentsline {subsubsection}{Gr\'afico de Tareas}{66}{section*.21}
\contentsline {subsection}{\numberline {7.1.7}Sprint 7}{66}{subsection.7.1.7}
\contentsline {subsubsection}{Gr\'afico de Esfuerzo}{66}{section*.22}
\contentsline {subsubsection}{Gr\'afico Individual}{67}{section*.23}
\contentsline {subsubsection}{Gr\'afico de Tareas}{67}{section*.24}
\contentsline {chapter}{Bibliograf\'{\i }a}{68}{figure.7.21}
