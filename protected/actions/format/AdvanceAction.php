<?php

class AdvanceAction extends CAction {

    public function run($id) {
        $proyect = ProyectModel::model()->findByPk($id);
        $model = new AdvanceForm();
        $m = new ProyectModel();
        if (isset($_POST['AdvanceForm'])) {
            $model->attributes = $_POST['AdvanceForm'];
            $m->id = $proyect->id;
            $m->title = $proyect->title;
            $m->tutor = $proyect->tutor;
            $m->cotutor = $proyect->cotutor;
            $m->jury1 = $proyect->jury1;
            $m->jury2 = $proyect->jury2;
            $m->proposal = $proyect->proposal;
            $m->semester = $proyect->semester;
            $m->status = $proyect->status;
            $m->department = $proyect->department;
            $proyect->date_present_advance = $model->date_present_advance;
            $proyect->porcentage = $model->porcentage;
            $proyect->observations = $model->observations;
            $proyect->status = '9';
            // echo json_encode ( $proyect->getAttributes () );
            // echo json_encode ( $model->getAttributes () );
            // echo json_encode ( $m->getAttributes () );
            // exit ();
            if ($proyect->save()) Yii::app()->controller->widget('ext.easy-mail.Mail', 
                array(
                    'view' => 'advanceView',
                    'params' => array(
                        'to' => array(
                            $proyect->get_email($proyect->student_id) => $proyect->get_name($proyect->student_id)
                        ),
                        'content' => array(
                            'status' => $proyect->get_status($proyect->status),
                            'department' => $proyect->get_department($proyect->department),
                            // 'student_id' => $l->get_name($l->student_id),
                            // 'obsdepartment' => $proyect->obsdepartment,
                            'porcentage' => $proyect->porcentage,
                            'observations' => $proyect->observations,
                            'date_present_advance' => preg_replace('/\//', ' de ', 
                                Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $proyect->date_present_advance))
                        ),
                        'subject' => 'Notificación de Registro de Avance'
                    )
                ));
            $this->controller->redirect(Yii::app()->createAbsoluteUrl("format/advanceprint/{$proyect->id}"));
        }
        $this->controller->render('advance', 
            array(
                'model' => $model,
                'proyect' => $proyect,
                'student' => $proyect->student->id0
            ));
    }

}