<?php

class AssignJuryAction extends CAction {

    public function run($id) {
        $model = $this->controller->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->controller->performAjaxValidation($model);
        if (isset($_POST['TeacherProyectModel'])) {
            $model->attributes = $_POST['TeacherProyectModel'];
            // echo json_encode($model->getAttributes());
            // exit();
            if ($model->save()) $this->controller->redirect(Yii::app()->createAbsoluteUrl('proyect/admin'));
        }
        Yii::app ()->user->setFlash ( 'info', 'Jurado Asignado' );
        $this->controller->render('update', array(
            'model' => $model
        ));
    }

}

