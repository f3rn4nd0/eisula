<?php

class ChangetitleAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
            $model->status = "3";
            if ($model->update()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
            Yii::app()->user->setFlash('info', 'Cambio de Título Exitoso');
        }
        $this->controller->render('changetitle', array(
            'model' => $model
        ));
    }

}