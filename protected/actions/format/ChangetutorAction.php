<?php

class ChangetutorAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
            $model->status = "3";
            if ($model->update()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('changetutor', array(
            'model' => $model
        ));
    }

}
