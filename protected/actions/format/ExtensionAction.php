<?php

class ExtensionAction extends CAction {

    public function run($id = NULL) {
        if ($id) {
            $proyect = ProyectModel::model()->findByfindByPk($id);
        } else {
            $proyect = ProyectModel::model()->findByAttributes(
                array(
                    'status' => 9,
                    'student_id' => Yii::app()->user->id
                ));
        }
        $setting = SystemSetting::model()->findByPk(1);
        $model = new ExtensionForm();
        $user = new UserModel();
        if (Yii::app()->request->isPostRequest) {
            $model->next_semester = $setting->next_semester;
            $user = new UserModel();
            $proyect->status = '11';
//             echo json_encode($model->getAttributes());
//             echo json_encode($proyect->getAttributes());
//             exit();
            if ($proyect->update()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('extension', 
            array(
                'model' => $model,
                'proyect' => $proyect,
                'user' => $user
            ));
    }

}