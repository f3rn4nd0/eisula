<?php

class FinalAction extends CAction {

    public function run($id) {
        $proyect = ProyectModel::model()->findByPk($id);
        $model = new FinalForm();
        if (isset($_POST['FinalForm'])) {
            $model->attributes = $_POST['FinalForm'];
            $proyect->date_present = $model->date_present;
            $proyect->status = '12';
            $proyect->score_tutor = $_POST['score_tutor'];
            $proyect->score_manuscript = $_POST['score_manuscript'];
            $proyect->score_exposure = $_POST['score_manuscript'];
            $proyect->score_product = $_POST['score_product'];
            $proyect->score_final = round(
                ((($_POST['score_tutor']) *
                     .3) +
                     (($_POST['score_manuscript']) *
                     .3) +
                     (($_POST['score_exposure']) *
                     .2) +
                     (($_POST['score_product']) *
                     .2)));
            // echo json_encode($_POST) . "<br>";
            // echo json_encode($proyect->attributes);
            // exit();
            if ($proyect->save()) $this->controller->redirect(
                Yii::app()->createAbsoluteUrl("format/finalprint/{$proyect->id}"));
        }
        $this->controller->render('final', 
            array(
                'model' => $model,
                'proyect' => $proyect,
                'student' => $proyect->student->id0
            ));
    }

}