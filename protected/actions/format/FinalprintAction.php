<?php

class FinalprintAction extends CAction {

    public function run($id) {
        $proyect = ProyectModel::model()->findByPk($id);
        $model = new FinalForm();
        if (isset($_POST['FinalForm'])) {
            $model->attributes = $_POST['FinalForm'];
            $this->controller->redirect(array(
                'proyect/admin'
            ));
        }
        $this->controller->render('finalprint', 
            array(
                'model' => $model,
                'proyect' => $proyect,
                'student' => $proyect->student->id0
            ));
    }

}