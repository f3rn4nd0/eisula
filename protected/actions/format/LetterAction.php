<?php

class LetterAction extends CAction {

    public function run() {
        $modelo = new UserModel();
        $model = new LetterForm();
        $setting = SystemSetting::model()->findByPk(1);
        $model->semester = $setting->semester;
        if (isset($_POST['LetterForm'])) {
            try {
                $transaction = Yii::app()->db->beginTransaction();
                $model->attributes = $_POST['LetterForm'];
                $l = new ProyectModel();
                $l->title = $model->title;
                $l->semester = $model->semester;
                $l->priority = $model->matter;
                $l->student_id = Yii::app()->user->id;
                $l->status = "0";
                $l->tutor = $model->tutor;
                if ($l->save()) {
                    Yii::app()->controller->widget('ext.easy-mail.Mail', 
                        array(
                            'view' => 'letterView',
                            'params' => array(
                                'to' => array(
                                    $l->get_email($model->tutor) => $l->get_name($model->tutor)
                                ),
                                'content' => array(
                                    'title' => $l->title,
                                    'student_id' => $l->get_name($l->student_id),
                                    'id' => $l->id
                                ),
                                'subject' => 'Notificación de Proyecto de Grado'
                            )
                        ));
                    $transaction->commit();
                    $this->controller->redirect(Yii::app()->createAbsoluteUrl("format/letterprint/{$l->id}"));
                }
            } catch (Exception $e) {
                $transaction->rollback();
            }
        }
        $this->controller->render('letter', array(
            'model' => $model
        ));
    }

}