<?php

class letterprintAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
        }
        $this->controller->render('letterprint', array(
            'model' => $model
        ));
    }

}
