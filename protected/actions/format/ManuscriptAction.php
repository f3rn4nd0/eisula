<?php

class ManuscriptAction extends CAction {

    public function run($id) {
        $proyect = ProyectModel::model()->findByAttributes(
            array(
                'status' => 12,
                'student_id' => Yii::app()->user->id
            ));
        $model = new ManuscriptForm();
        if (isset($_POST['ManuscriptForm'])) {
            $model->attributes = $_POST['ManuscriptForm'];
            $proyect->student_id = Yii::app()->user->id;
            $proyect->status = "13";
            // TRATAMIENTO DEL ARCHIVO
            // $proyect->manuscript= RUTA WEB;
            $file = CUploadedFile::getInstance($model, 'manuscript');
            $dir = Yii::app()->basePath .
                 DIRECTORY_SEPARATOR .
                 '..' .
                 DIRECTORY_SEPARATOR .
                 'docs' .
                 DIRECTORY_SEPARATOR .
                 Yii::app()->user->id;
            if (! file_exists($dir)) {
                @mkdir($dir, 0775, true);
            }
            $file->saveAs($dir . DIRECTORY_SEPARATOR . $file->name);
            $proyect->manuscript = '/docs/' . Yii::app()->user->id . '/' . $file->name;
//             echo json_encode($proyect->getAttributes());
//             echo json_encode($_POST);
//             echo json_encode($_FILES);
//             exit();
            if ($proyect->save()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('manuscript', 
            array(
                'model' => $model,
                'proyect' => $proyect
            ));
    }
   
}