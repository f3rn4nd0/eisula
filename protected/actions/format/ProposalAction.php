<?php

class ProposalAction extends CAction {

    public function run() {
        $proyect = ProyectModel::model()->find(
            array(
                'condition' => 'student_id=:t0 AND (status=:t1 OR status=:t2 OR status=:t3)',
                'params' => array(
                    ':t0' => Yii::app()->user->id,
                    ':t1' => '1',
                    ':t2' => '5',
                    ':t3' => '7'
                )
            )
//             array(
//                 'status' => "1",
//                 'student_id' => Yii::app()->user->id
            );
        $model = new ProposalForm();
        if (isset($_POST['ProposalForm'])) {
            $model->attributes = $_POST['ProposalForm'];
            $proyect->agency = $model->agency;
            $proyect->cotutor = $model->cotutor;
            $proyect->student_id = Yii::app()->user->id;
            $proyect->department = $model->department;
            $proyect->status = "3";
            // TRATAMIENTO DEL ARCHIVO
            // $proyect->proposal= RUTA WEB;
            $file = CUploadedFile::getInstance($model, 'annexes');
            $dir = Yii::app()->basePath .
                 DIRECTORY_SEPARATOR .
                 '..' .
                 DIRECTORY_SEPARATOR .
                 'docs' .
                 DIRECTORY_SEPARATOR .
                 Yii::app()->user->id;
            if (! file_exists($dir)) {
                @mkdir($dir, 0775, true);
            }
            $file->saveAs($dir . DIRECTORY_SEPARATOR . $file->name);
            $proyect->proposal = '/docs/' . Yii::app()->user->id . '/' . $file->name;
            // echo json_encode($proyect->getAttributes());
            // echo json_encode($_POST);
            // echo json_encode($_FILES);
            // exit();
            if ($proyect->save()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('proposal', 
            array(
                'model' => $model,
                'proyect' => $proyect
            ));
    }

}