<?php

class ProposaldeptoAction extends CAction {

    public function run($id) {
        $proyect = ProyectModel::model()->findByPk($id);
        $model = new ProposaldeptoForm();
        if (isset($_POST['ProposaldeptoForm'])) {
            $model->attributes = $_POST['ProposaldeptoForm'];
            // $proyect->student_id = Yii::app()->user->id;
            $proyect->status = "5";
            $proyect->obsdepartment = $model->obsdepartment;
            // TRATAMIENTO DEL ARCHIVO
            // $proyect->proposal= RUTA WEB;
            // $file = CUploadedFile::getInstance($model, 'annexes');
            // $dir = Yii::app()->basePath .
            // DIRECTORY_SEPARATOR .
            // '..' .
            // DIRECTORY_SEPARATOR .
            // 'docs' .
            // DIRECTORY_SEPARATOR .
            // Yii::app()->user->id;
            // if (! file_exists($dir)) {
            // @mkdir($dir, 0775, true);
            // }
            // $file->saveAs($dir . DIRECTORY_SEPARATOR . $file->name);
            // $proyect->proposal = '/docs/' . Yii::app()->user->id . '/' . $file->name;
            // echo json_encode($proyect->getAttributes());
            // echo json_encode($_POST);
            // echo json_encode($_FILES);
            // exit();
            if ($proyect->save()) {
                Yii::app()->controller->widget('ext.easy-mail.Mail', 
                    array(
                        'view' => 'proposaldeptoView',
                        'params' => array(
                            'to' => array(
                                $proyect->get_email($proyect->student_id) => $proyect->get_name($proyect->student_id)
                            ),
                            'content' => array(
                                'status' => $proyect->get_status($proyect->status),
                                'department' => $proyect->get_department($proyect->department),
                                // 'student_id' => $l->get_name($l->student_id),
                                'obsdepartment' => $proyect->obsdepartment
                            ),
                            'subject' => 'Notificación de Decisión del Departamento'
                        )
                    ));
                $this->controller->redirect(Yii::app()->createAbsoluteUrl('proyect/admin'));
            }
        }
        $this->controller->render('proposaldepto', array(
            'model' => $model,
            'proyect' => $proyect
        ));
    }

}