<?php

class ReqAction extends CAction {

    public function run($nota1, $nota2, $nota3, $nota4) {
        $nota = $nota1;
        $letras = "NOTA EN LETRAS";
        $score_final = $nota;
        $notaF = round(($nota1 + $nota2 + $nota3 + $nota4));
        echo CJSON::encode(
            array(
                'nota' => round(($nota1 + $nota2 + $nota3 + $nota4)),
                'score_final' => round(($nota1 + $nota2 + $nota3 + $nota4)),
                'letras' => Numeric::num_to_letras($notaF, 'PUNTOS', null)
            ));
        // Yii::app()->end();
    }

}
