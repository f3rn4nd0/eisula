<?php

class AdminAction extends CAction {

    public function run() {
        $model = new ProyectModel('search');
        $model->unsetAttributes(); // clear any default values
        $model->setAttributes(Yii::app()->request->getParam('ProyectModel'), false);
//         echo json_encode($model->getAttributes());
//         if (isset($_GET['ProyectModel'])) $model->attributes = $_GET['ProyectModel'];
        $this->controller->render('admin', array(
            'model' => $model
        ));
    }

}