<?php

class CheckAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        $model->status = '4';
        $model->update();
        Yii::app()->user->setFlash('info', 'Proyecto Aprobado');
        $this->controller->redirect(Yii::app()->createAbsoluteUrl('proyect/admin'));
    }

}