<?php

class CheckDirAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        $model->status = '6';
        $model->update();
        Yii::app()->controller->widget('ext.easy-mail.Mail', 
            array(
                'view' => 'checkDirView',
                'params' => array(
                    'to' => array(
                        $model->get_email($model->student_id) => $model->get_name($model->student_id)
                    ),
                    'content' => array(
                        'department' => $model->get_department($model->department),
                        'status' => $model->get_status($model->status),
                        'jury1' => $model->get_name($model->jury1),
                        'emailj1' => $model->get_email($model->jury1),
                        'jury2' => $model->get_name($model->jury2),
                        'emailj2' => $model->get_email($model->jury2),
                        'jury3' => $model->get_name($model->jury3),
                        'emailj3' => $model->get_email($model->jury3)
                    ),
                    'subject' => 'Notificación de Decisión de la Dirección de la EISULA'
                )
            ));
        Yii::app()->user->setFlash('info', 'Proyecto Aprobado');
        $this->controller->redirect(Yii::app()->createAbsoluteUrl('proyect/admin'));
    }

}