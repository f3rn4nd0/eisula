<?php
class CreateAction extends CAction{
    public function run(){
        $model = new ProyectModel();
        // Uncomment the following line if AJAX validation is needed
        // $this->controller->performAjaxValidation($model);
        if (isset($_POST['Proyectmodel'])) {
            $model->attributes = $_POST['ProyectModel'];
            if ($model->save()) $this->controller->redirect(
                array(
                    'view',
                    'id' => $model->id
                ));
        }
        $this->controller->render('create', array(
            'model' => $model
        ));
    }
}