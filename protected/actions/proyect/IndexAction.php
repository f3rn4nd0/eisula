<?php
class IndexAction extends CAction{
    public function run(){
        $dataProvider = new CActiveDataProvider('ProyectModel');
        $this->controller->render('index', array(
            'dataProvider' => $dataProvider
        ));
    }
}