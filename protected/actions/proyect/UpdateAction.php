<?php

class UpdateAction extends CAction {

    public function run($id) {
        $model = $this->controller->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->controller->performAjaxValidation($model);
        $model->status = '4';
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
            // echo json_encode($model->getAttributes());
            // exit();
            if ($model->save()) Yii::app()->controller->widget('ext.easy-mail.Mail', 
                array(
                    'view' => 'assignjuryView',
                    'params' => array(
                        'to' => array(
                            $model->get_email($model->student_id) => $model->get_name($model->student_id)
                        ),
                        'content' => array(
                            'department' => $model->get_department($model->department),
                            'status' => $model->get_status($model->status),
                            'jury1' => $model->get_name($model->jury1),
                            'emailj1' => $model->get_email($model->jury1),
                            'jury2' => $model->get_name($model->jury2),
                            'emailj2' => $model->get_email($model->jury2),
                            'jury3' => $model->get_name($model->jury3),
                            'emailj3' => $model->get_email($model->jury3)
                        // 'obsdepartment' => $proyect->obsdepartment
                                                ),
                        'subject' => 'Notificación de Decisión del Departamento'
                    )
                ));
            Yii::app()->user->setFlash('info', 'Jurado Asignado');
            $this->controller->redirect(Yii::app()->createAbsoluteUrl('proyect/admin'));
        }
        $this->controller->render('update', array(
            'model' => $model
        ));
    }

}