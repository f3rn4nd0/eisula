<?php
class RegisterAdmAction extends CAction {
	public function run() {
		$model = new UserModel ();
		if (isset ( $_POST ['UserModel'] )) {
			$model->attributes = $_POST ['UserModel'];
			$model->type = '3';
			if ($model->save ()) {
				Yii::app ()->controller->widget ( 'ext.easy-mail.Mail', array (
						'view' => 'regView',
						'params' => array (
								'to' => array (
										"$model->email" => "$model->name $model->last_name" 
								),
								'content' => array (
										'dni' => $model->dni 
								),
								'subject' => 'Registro de Usuario' 
						) 
				) );
			}
			// echo json_encode($model->getAttributes());
			// exit();
			Yii::app()->user->setFlash('info', 'Registro Completado');
			$this->controller->redirect ( Yii::app ()->user->returnUrl );
		}
		$this->controller->render ( 'registeradm', array (
				'model' => $model 
		) );
	}
}