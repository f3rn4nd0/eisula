<?php
class RegisterEstAction extends CAction {
	public function run() {
		$model = new UserModel ();
		if (isset ( $_POST ['UserModel'] )) {
			$model->attributes = $_POST ['UserModel'];
			$model->type = '1';
			if ($model->save ()) {
				$s = new StudentModel ();
				$s->id = $model->id;
				$s->save ();
				Yii::app ()->controller->widget ( 'ext.easy-mail.Mail', array (
						'view' => 'regView',
						'params' => array (
								'to' => array (
										"$model->email" => "$model->name $model->last_name" 
								),
								'content' => array (
										'dni' => $model->dni 
								),
								'subject' => 'Registro de Usuario' 
						) 
				) );
				Yii::app()->user->setFlash('info', 'Registro Completado');
				$this->controller->redirect ( Yii::app ()->user->returnUrl );
			}
		}
		$this->controller->render ( 'registerest', array (
				'model' => $model 
		) );
	}
}