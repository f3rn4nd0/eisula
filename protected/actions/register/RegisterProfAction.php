<?php

class RegisterProfAction extends CAction {

    public function run() {
        $model = new UserModel();
        if (isset($_POST['UserModel'])) {
            $model->attributes = $_POST['UserModel'];
            $model->type = '2';
            if ($model->save()) {
                $pr = new TeacherModel();
                $pr->id = $model->id;
                
                $file = CUploadedFile::getInstance($model, 'ext');
                $dir = Yii::app()->basePath .
                     DIRECTORY_SEPARATOR .
                     '..' .
                     DIRECTORY_SEPARATOR .
                     'docs1' .
                     DIRECTORY_SEPARATOR .
                     Yii::app()->user->id;
                if (! file_exists($dir)) {
                    @mkdir($dir, 0775, true);
                }
                
                
                $file->saveAs($dir . DIRECTORY_SEPARATOR . $file->name);
                $pr->curriculum = '/docs1/' . Yii::app()->user->id . '/' . $file->name;

                if ($pr->save()) {
                    Yii::app()->controller->widget('ext.easy-mail.Mail', 
                        array(
                            'view' => 'regView',
                            'params' => array(
                                'to' => array(
                                    "$model->email" => "$model->name $model->last_name"
                                ),
                                'content' => array(
                                    'dni' => $model->dni
                                ),
                                'subject' => 'Registro de Usuario'
                            )
                        ));
                    Yii::app()->user->setFlash('info', 'Registro Completado');
                    $this->controller->redirect(Yii::app()->user->returnUrl);
                }
            }
        }
        $this->controller->render('registerprof', array(
            'model' => $model
        ));
    }

}