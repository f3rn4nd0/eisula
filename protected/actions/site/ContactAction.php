<?php

class ContactAction extends CAction {

    public function run() {
        $model = new ContactForm();
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                Yii::app()->params['adminemail'];
                Yii::app()->controller->widget('ext.easy-mail.Mail', 
                    array(
                        'view' => 'contactview',
                        'params' => array(
                            'to' => array(
                                Yii::app()->params['adminemail'] => Yii::app()->name
                            ),
                            'from' => array(
                                'email' => $model->email,
                                'name' => $model->name
                            ),
                            'body' => array(
                                'body' => $model->body
                            ),
                            'content' => array(
                                'name' => $model->name,
                                'subject' => $model->subject,
                                'email' => $model->email
                            ),
                            'subject' => $model->subject
                        )
                    ));
                
                $this->controller->redirect(Yii::app()->user->returnUrl);
            }
            Yii::app()->user->setFlash('info',
            'Gracias por contactarnos. Le responderemos a la mayor brevedad posible.');
        }
        
        $this->controller->render('contact', array(
            'model' => $model
        ));
    }

}


