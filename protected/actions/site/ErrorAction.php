<?php
class ActionError extends CAction{
    public function run(){
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) echo $error['messages'];
            else $this->controller->render('error', $error);
        }
    }
}