<?php

class IndexAction extends CAction {

    public function run() {
        switch (Yii::app()->user->getState('type')) {
            case '0':
            case '3':
                $model = new UserModel('search');
                $model->setAttributes(Yii::app()->request->getParam('UserModel'));
                $model->type = isset($_REQUEST['UserModel']) ? $_REQUEST['UserModel']['type'] : null;
                $this->controller->render('index3', 
                    array(
                        'model' => $model
                    ));
            break;
            case '1':
                $model = new ProyectModel('search');
                $model->unsetAttributes(); // clear any default values
                if (isset($_GET['ProyectModel'])) $model->attributes = $_GET['ProyectModel'];
                $this->controller->render('index1', array(
                    'model' => $model
                ));
            break;
            case '2':
                $model = new ProyectModel('search');
                $model->unsetAttributes(); // clear any default values
                if (isset($_GET['ProyectModel'])) $model->attributes = $_GET['ProyectModel'];
                // $model->findAll(User::model()->findAll(array('condition'=>Yii::app()->user->getId('id=teacher_id'))));
                $this->controller->render('index2', array(
                    'model' => $model
                ));
            break;
            case '4':
                $model = new UserModel('search');
                $model->setAttributes(Yii::app()->request->getParam('UserModel'));
                $model->type = isset($_REQUEST['UserModel']) ? $_REQUEST['UserModel']['type'] : null;
                $this->controller->render('index3',
                    array(
                        'model' => $model
                    ));
                break;
            default:
                $this->controller->render('index');
        }
    }

}