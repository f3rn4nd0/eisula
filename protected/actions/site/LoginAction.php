<?php
class LoginAction extends CAction{
    public function run(){
       // Yii::app()->user->setFlash('success', 'Your content...');
        $model = new LoginForm();
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('login', array(
            'model' => $model
        ));
    }
    
}