<?php

class PasswordAction extends CAction {
	public function run() {
		$model = new Password();
		$model->dni = Yii::app()->request->getParam('dni');
		$model->token = Yii::app()->request->getParam('token');
		if (isset ( $_POST ["Password"] )) {
			$model->setAttributes(Yii::app()->request->getParam(get_class($model)));
			if ($model->save()) {
				$this->controller->redirect ( Yii::app ()->homeUrl );
			}
		}
		$this->controller->render ( 'password', array (
				'model' => $model 
		) );
	}
}