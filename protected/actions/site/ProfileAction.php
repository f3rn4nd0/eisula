<?php

class ProfileAction extends CAction {

    public function run() {
        $model = UserModel::model()->findByPk(Yii::app()->user->id);
        $model->password = null;
        if (isset($_POST["ProfileForm"])) {
            $model->setAttribute('email', $_POST["ProfileForm"]["email"]);
            $model->setAttribute('password', $_POST["ProfileForm"]["password"]);
            if ($model->save()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('profile', array(
            'model' => $model
        ));
    }

}