<?php

class RecoveryAction extends CAction {

    public function run() {
        $model = new Recovery();
        if (isset($_POST["Recovery"])) {
            $model->attributes = $_POST['Recovery'];
            if ($model->validate()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('recovery', array(
            'model' => $model
        ));
    }

}