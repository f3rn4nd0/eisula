<?php

class Update_PassAction extends CAction {
	public function run() {
		$model = new Update_Pass();
		//$model->dni = Yii::app()->request->getParam('dni');
		if (isset ( $_POST ["Update_Pass"] )) {
			$model->setAttributes(Yii::app()->request->getParam(get_class($model)));
			if ($model->save()) {
				$this->controller->redirect ( Yii::app ()->homeUrl );
			}
		}
		$this->controller->render ( 'update_pass', array (
				'model' => $model 
		) );
	}
}