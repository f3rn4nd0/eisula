<?php

class ViewDetailsAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
            if ($model->update()) {
                Yii::app()->controller->widget('ext.easy-mail.Mail', 
                    array(
                        'view' => 'viewdetailsView',
                        'params' => array(
                            'to' => array(
                                $model->get_email($model->student_id) => $model->get_name($model->student_id)
                            ),
                            'content' => array(
                                'title' => $model->title,
                                'tutor' => $model->get_name($model->tutor),
                                'student_id' => $model->get_name($model->student_id),
                                'status' => $model->get_status()
                            ),
                            'subject' => 'Decisión del Tutor Sobre el Proyecto '
                        )
                    ));
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('viewDetails', array(
            'model' => $model
        ));
    }

} 