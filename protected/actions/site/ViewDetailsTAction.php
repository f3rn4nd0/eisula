<?php

class ViewDetailsTAction extends CAction {

    public function run($id) {
        $model = ProyectModel::model()->findByPk($id);
        if (isset($_POST['ProyectModel'])) {
            $model->attributes = $_POST['ProyectModel'];
            if ($model->update()) {
                $this->controller->redirect(Yii::app()->homeUrl);
            }
        }
        $this->controller->render('viewDetailsT', array(
            'model' => $model
        ));
    }

} 