<?php

class Controller extends CController {
    public $layout = '//layouts/content';
    public $menu = array();
    public $breadcrumbs = array();

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete'
        );
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array(
                    '@'
                )
            ),
            array(
                'deny',
                'users' => array(
                    '*'
                )
            )
        );
    }

    public function init() {
        switch (Yii::app()->user->getState('type')) {
            case '0':
                $this->menu = array(
                    array(
                        'label' => 'Crear Usuario',
                        'url' => Yii::app()->createAbsoluteUrl('user/create')
                    ),
                    array(
                        'label' => 'Listar Usuarios',
                        'url' => Yii::app()->createAbsoluteUrl('user/admin')
                    ),
                    array(
                        'label' => 'Listar Proyectos',
                        'url' => Yii::app()->createAbsoluteUrl('proyect/admin')
                    ),
                    array(
                        'label' => 'Configurar Sistema',
                        'url' => Yii::app()->createAbsoluteUrl('systemSetting')
                    )
                );
            break;
            case '3':
                $this->menu = array(
                    array(
                        'label' => 'Listar Usuarios',
                        'url' => Yii::app()->createAbsoluteUrl('user/admin')
                    ),
                    array(
                        'label' => 'Listar Proyectos',
                        'url' => Yii::app()->createAbsoluteUrl('proyect/admin')
                    )
                // array(
                // 'label' => 'Configurar Sistema',
                // 'url' => Yii::app()->createAbsoluteUrl('systemSetting')
                // )
                                );
            break;
            case '1':
                $this->menu = array(
                    array(
                        'label' => 'Lista de Proyectos',
                        'url' => Yii::app()->createAbsoluteUrl('site/index')
                    ),
                    array(
                        'label' => 'Descargar Reglamento',
                        'url' => Yii::app()->createAbsoluteUrl('/docs/ReglamentoProyectoGrado09Feb_2015.pdf'),
                        'linkOptions' => array(
                            'target' => '_blank'
                        )
                    ),
                    array(
                        'label' => 'Registrar Carta de Compromiso',
                        'url' => Yii::app()->createAbsoluteUrl('format/letter'),
                        'visible' => ! ProyectModel::model()->exists(
                            array(
                                'condition' => 'student_id=:t1 OR status=:t2',
                                'order' => '',
                                'params' => array(
                                    ':t1' => Yii::app()->user->id,
                                    ':t2' => '2'
                                )
                            ))
                    )
                );
            break;
            case '2':
                $this->menu = array(
                    array(
                        'label' => 'Lista de Proyectos',
                        'url' => Yii::app()->createAbsoluteUrl('site')
                    )
                );
            break;
            case '4':
                $this->menu = array(
                    array(
                        'label' => 'Crear Usuario',
                        'url' => Yii::app()->createAbsoluteUrl('user/create')
                    ),
                    array(
                        'label' => 'Lista de Usuarios',
                        'url' => Yii::app()->createAbsoluteUrl('user/admin')
                    ),
                    array(
                        'label' => 'Listar Proyectos',
                        'url' => Yii::app()->createAbsoluteUrl('proyect/admin')
                    ),
                    array(
                        'label' => 'Configurar Sistema',
                        'url' => Yii::app()->createAbsoluteUrl('systemSetting')
                    )
                );
            break;
        }
    }

}