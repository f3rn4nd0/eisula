<?php

class UserIdentity extends CUserIdentity {
    private $_id;

    public function authenticate() {
        $user = User::model()->find('LOWER(dni)=?', array(
            strtolower($this->username)
        ));
        if ($user === null || $user->password != md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
        }
        return ! $this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
    /*
     * array( 'demo'=>'demo', 'admin'=>'admin', 'admi'=>'123', ); if(!isset($users[$this->username]))
     * $this->errorCode=self::ERROR_USERNAME_INVALID; elseif($users[$this->username]!==$this->password)
     * $this->errorCode=self::ERROR_PASSWORD_INVALID; else $this->errorCode=self::ERROR_NONE; return !$this->errorCode;
     */
}


