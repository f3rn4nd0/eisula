<?php
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Sistema de Gestión de Proyectos de Grado de la EISULA',
    'charset' => 'UTF-8',
    'language' => 'es',
    'import' => array(
        'application.models.*',
        'application.models.base.*',
        'application.components.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '15825213'
        )
    ),
    'components' => array(
        'user' => array(
            'allowAutoLogin' => true,
            'authTimeout' => 60 * 10
        ),
//         'coreMessages' => array(
//             'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'messages'
//         ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
            )
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=eisula',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8'
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error'
        ),
        /*
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                )
            )
        )
        */
    ),
    /*
    'preload' => array(
        'log'
    ),
    */
    'params' => array(
				'adminemail'=>'sgpgeisula@gmail.com',
//                 clave=eisulamail
				)
		//require_once (dirname(__FILE__) . '/params.php')
);
