<?php

class FormatController extends Controller {

    public function actions() {
        return array(
            'advance' => 'application.actions.format.AdvanceAction',
            'advanceprint' => 'application.actions.format.AdvanceprintAction',
            'error' => 'application.actions.format.ErrorAction',
            'findCed' => 'application.actions.format.FindCedAction',
            'letter' => 'application.actions.format.LetterAction',
            'letterprint' => 'application.actions.format.LetterprintAction',
            'proposal' => 'application.actions.format.ProposalAction',
            'proposaldepto' => 'application.actions.format.ProposaldeptoAction',
            'proposaldir' => 'application.actions.format.ProposaldirAction',
            'final' => 'application.actions.format.FinalAction',
            'finalprint' => 'application.actions.format.FinalprintAction',
            'assignjury' => 'application.actions.format.AssignJuryAction',
        	'extension' => 'application.actions.format.ExtensionAction',
            'ajax_request' => 'application.actions.format.ReqAction',
            'manuscript' => 'application.actions.format.ManuscriptAction',
        	'changetitle' => 'application.actions.format.ChangetitleAction',
            'changetutor' => 'application.actions.format.ChangetutorAction',
            
        		 
        );
    }
    public function loadModel($id) {
        $model = ProyectModel::model()->findByPk($id);
        if ($model === null) throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
