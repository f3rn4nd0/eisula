<?php

class ProyectController extends Controller {

    public function actions() {
        return array(
            'admin' => 'application.actions.proyect.AdminAction',
            'create' => 'application.actions.proyect.CreateAction',
            'delete' => 'application.actions.proyect.DeleteAction',
            'index' => 'application.actions.proyect.IndexAction',
            'update' => 'application.actions.proyect.UpdateAction',
            'view' => 'application.actions.proyect.ViewAction',
            'check'=> 'application.actions.proyect.CheckAction',
            'checkdir'=> 'application.actions.proyect.CheckDirAction',
        );
    }

    public function loadModel($id) {
        $model = ProyectModel::model()->findByPk($id);
//         switch (Yii::app()->user->getState('type')) {
//             case '0' :
//             case '1' :
//                 if(Yii::app()->user->id != $model->$student_id)
//                     $model == null;
//                     break;
//             case '2' :
//                 if(Yii::app()->user->id != $model->$tutor)
//                     $model == null;
//                     break;
//         }
        if ($model === null) throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}
