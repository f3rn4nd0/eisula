<?php

class RegisterController extends Controller {

    public function actions() {
        return array(
            'registeradm' => 'application.actions.register.RegisterAdmAction',
            'registerest' => 'application.actions.register.RegisterEstAction',
            'registerprof' => 'application.actions.register.RegisterProfAction',
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF
            )
        );
    }
    
public function accessRules() {
        return array(
            // PERMITE ACCESO A USUARIOS INVITADO
            array(
                'allow',
                'actions' => array(
                    'login',
                    'index',
                    'error',
                    'captcha',
                    'contact',
                    'recovery',
                    'password',
                    'update_pass',
                	'registeradm',
                	'registerest',
                	'registerprof'
                ),
                'users' => array(
                    '*'
                )
            ),
            // PERMITE ACCESO A USUARIOS AUTENTICADO
            array(
                'allow',
                'actions' => array(
                    'logout',
                    'viewDetails',
                    'viewDetailsst',
                    'viewDetailst'
                    
                ),
                'users' => array(
                    '@'
                )
            ),
            // DENEGO ACCESO A USUARIOS INVITADOS
            array(
                'deny',
                'users' => array(
                    '*'
                )
            ),
            // DENEGO ACCESO A USUARIOS AUTENTICADO
            array(
                'deny',
                'actions' => array(
                    'login'
                ),
                'users' => array(
                    '@'
                )
            ),
        );
    }

}