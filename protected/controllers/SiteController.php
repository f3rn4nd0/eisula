<?php

class SiteController extends Controller {

    public function actions() {
        return array(
            'index' => 'application.actions.site.IndexAction',
            'viewDetails' => 'application.actions.site.ViewDetailsAction',
            'viewDetailsT' => 'application.actions.site.ViewDetailsTAction',
            'viewDetailsST' => 'application.actions.site.ViewDetailsSTAction',
            'error' => 'application.actions.site.ErrorAction',
            'contact' => 'application.actions.site.ContactAction',
            'login' => 'application.actions.site.LoginAction',
            'logout' => 'application.actions.site.LogoutAction',
            'recovery' => 'application.actions.site.RecoveryAction',
            'password' => 'application.actions.site.PasswordAction',
            'update_pass' => 'application.actions.site.Update_PassAction',
            'profile' => 'application.actions.site.ProfileAction',
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF
            )
        );
    }

    public function accessRules() {
        return array(
            // PERMITE ACCESO A USUARIOS INVITADO
            array(
                'allow',
                'actions' => array(
                    'login',
                    'index',
                    'error',
                    'captcha',
                    'contact',
                    'recovery',
                    'password',
                    'update_pass',
                    'registeradm',
                    'registerest',
                    'registerprof'
                ),
                'users' => array(
                    '*'
                )
            ),
            // PERMITE ACCESO A USUARIOS AUTENTICADO
            array(
                'allow',
                'actions' => array(
                    'logout',
                    'viewDetails',
                    'viewDetailsT',
                    'viewDetailsST',
                	'changetitle',
                    'profile'
                ),
                'users' => array(
                    '@'
                )
            ),
            // DENEGO ACCESO A USUARIOS INVITADOS
            array(
                'deny',
                'users' => array(
                    '*'
                )
            ),
            // DENEGO ACCESO A USUARIOS AUTENTICADO
            array(
                'deny',
                'actions' => array(
                    'login'
                ),
                'users' => array(
                    '@'
                )
            )
        );
    }

}
