Estimad@ bachiller, le informamos que el
<b>Departamento de <?php echo $params['content']['department']?></b>
, ya ha registrado el
<b>AVANCE</b>
de su Proyecto de grado, el mismo para la fecha de la presentación
<b>(<?php echo $params['content']['date_present_advance']?>)</b>
cuenta con un progreso del
<b><?php echo $params['content']['porcentage']?> %</b>.
<br>
<br>
Las Observaciones hechas por los miembros del Jurado son las siguientes: <p>
<?php echo $params['content']['observations']?> 
