Estimad@ Bachiller, la propuesta que usted sometio ante el
<b>Departamento de <?php echo $params['content']['department']?></b>
, ya fue discutida por la
<b> Dirección de la EISULA</b>
y el
<b> Estatus</b>
de la misma es:
<b><?php echo $params['content']['status']?></b>
.
<br>
<br>
Se le recuerda prepapar el avance del mismo para ser presentado ante el
jurado asignado por el
<b>Departamento de <?php echo $params['content']['department']?></b>
.
