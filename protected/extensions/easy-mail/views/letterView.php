Estimad@ Profesor@, Usted tiene un proyecto por aceptar como
<b>Tutor</b>
en el
<b>Sistema De Gestión de Proyectos de Grado de la Escuela de Ingeniería
	de Sistemas de Universiadad de Los Andes</b>
, la información del mismo se muestra a continación:
<br>
<br>
Titulo:
<b><?php echo $params['content']['title']; ?></b>
<br>
<br>

Bachiller:
<b><?php echo $params['content']['student_id']?></b>
<br>
<br>
Puede Consultar la información del mismo en el siguiente link:
<b><?php echo CHtml::link("Proyecto",Yii::app()->createAbsoluteUrl("site/viewDetails/{$params['content']['id']}"))?></b>
