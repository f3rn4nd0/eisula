Estimad@ Bachiller, la propuesta que usted sometio ante el
<b>Departamento de <?php echo $params['content']['department']?></b>
, ya fue discutida por este cuerpo y el
<b> Estatus</b>
de la misma es:
<b><?php echo $params['content']['status']?></b>
.
<br>
<br>
Se recomienda realizar las siguientes correcciones:
<b><?php echo $params['content']['obsdepartment']?></b>
; e introducir nuevamente la propuesta con las respectivas correcciones
para su posterior discución.

