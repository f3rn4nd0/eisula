Bienvenid@ <?php Yii::app()->user->getState('name') . ' ' .Yii::app()->user->getState('last_name')?>,
has solicitado el Reinicio de su clave de acceso al
<b>Sistema De Gestión de Proyectos de Grado de la Escuela de Ingeniería
de Sistemas de Universiadad de Los Andes</b>

<br>

Para Reestablecer su contraseña haga click en el siguiente enlace: 

 <?php echo CHtml::link('Reestablecer Contraseña', Yii::app()->createAbsoluteUrl('site/password', array(
     'dni' => $params['content']['dni'],     
     'token' => $params['content']['token']))); ?>
<br>