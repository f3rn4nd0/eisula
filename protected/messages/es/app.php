<?php
/*	model.attribute => value	*/

return array(

	'home'=>'Inicio',

	'options'=>'Opciones',

	'view {model} #{item}'=>'Ver {model} #{item}',

	'create'=>'Crear',
	'create {model}'=>'Crear {model}',
	
	'update'=>'Modificar',
	'update {model}'=>'Modificar {model}',
	'update {model} #{item}'=>'Modificar {model} #{item}',

	'delete {model}'=>'Borrar {model}',

	'index {model}'=>'Listar {model}',

	'admin {model}'=>'Administrar {model}',

);