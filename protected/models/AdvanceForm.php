<?php

class AdvanceForm extends CFormModel {
    public $id;
    public $title;
    public $date_present;
    public $student;
    public $porcentage;
    public $proyect_id;
    public $observations;
    public $tutor;
    public $cotutor;
    public $jury1;
    public $jury2;
    public $date_present_advance;

    public function rules() {
        return array(
            array(
                'title, date_present, date_present_advance,porcentage, proyect_id, jury1, jury2, proyect, observations, tutor, cotutor',
                'required',
//             'proyect, observations, tutor, cotutor',
//             'safe'
                        )
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Titulo:',
            'date_present' => 'Fecha de Presentación',
            'porcentage' => 'Porcentaje',
            'file' => 'Archivo',
            'proyect_id' => 'Proyecto',
            'observations' => 'Observaciones',
            'date_present_advance' => 'Fecha Presentación de Avance'
        );
    }

}