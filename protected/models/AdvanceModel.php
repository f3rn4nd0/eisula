<?php
class AdvanceModel extends Advance {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_present, porcentage, file, acta, proyect_id', 'required'),
            array('proyect_id', 'numerical', 'integerOnly'=>true),
            array('porcentage', 'length', 'max'=>30),
            array('file, acta', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, date_present, porcentage, file, acta, proyect_id', 'safe', 'on'=>'search'),
        );
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'proyect' => array(self::BELONGS_TO, 'Proyect', 'proyect_id'),
        );
    }
    
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date_present' => 'Fecha de Presentación',
            'porcentage' => 'Porcentaje de Avance',
            'file' => 'File',
            'acta' => 'Acta',
            'proyect_id' => 'Proyecto',
        );
    }
}