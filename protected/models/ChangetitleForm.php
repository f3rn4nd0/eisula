<?php

class ChangetitleForm extends CFormModel {
    public $title;
    
    public function rules() {
        return array(
            array(
                'title',
                'required'
            ), 
        );
    }

    public function attributeLabels() {
        return array(
            'title' => 'Titulo',
        );
    }
    
    public function get_name($id) {
    	if ($user = User::model()->findByPk($id)) {return $user->last_name . ', ' . $user->name;}
    }

}