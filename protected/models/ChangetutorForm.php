<?php

class ChangetutorForm extends CFormModel {
    public $tutor;
    public $cotutor;

    public function rules() {
        return array(
            array(
                'tutor',
                'required'
            ),array('cotutor','safe')
        );
    }

    public function attributeLabels() {
        return array(
            'tutor' => 'Tutor',
            'cotutor' => 'CoTutor',
        );
    }

    public function get_name($id) {
        if ($user = User::model()->findByPk($id)) {return $user->last_name . ', ' . $user->name;}
    }

}