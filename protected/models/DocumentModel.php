<?php
class DocumentModel extends Document
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'version' => 'Version',
			'date_preset' => 'Date Preset',
			'acta_final' => 'Acta Final',
			'proyect_id' => 'Proyect',
		);
	}

}