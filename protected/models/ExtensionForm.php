<?php
class ExtensionForm extends CFormModel {
	public $title;
	public $tutor;
	public $porcentage;
	public $date_present;
	public $cotutor;
	public $proyect_id;
	public $next_semester;
	public function rules() {
		return array (
				array (
						'tutor, title, porcentage, date_present',
						'required' 
				),
				array (
						'cotutor',
						'safe' 
				) 
		);
	}
	
	public function attributeLabels() {
		return array(
				'tutor' => 'Nombre',
				'cotutor' => 'Cotutor',
				'title' => 'Titulo',
				'porcentage' => 'Porcentaje de Avance',
				'date_present' => 'Fecha de Presentación de Avance',
				'next_semester' => 'Próximo Semestre'
		);
	}
	
	public function get_name($id) {
		if ($user = User::model()->findByPk($id)) {return $user->last_name . ', ' . $user->name;}
	}
}