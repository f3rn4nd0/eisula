<?php
class FinalForm extends CFormModel {
	// public $teacher_id;
	public $id;
	public $proyect_id;
	public $date_present;
	// public $dni;
	public $title;
	public $observations;
	public $status;
	public $score_tutor;
	public $score_manuscript;
	public $score_exposure;
	public $score_product;
	public $score_final;
	public function rules() {
		return array (
				array (
						' id, title, observations, date_present, status, score_final',
						'required' 
				),
				array (
						'score_tutor',
						'match',
						'pattern' => '/^[0-9]+$/i',
						'message' => 'Debe introducir solo Números' 
				),
				array (
						'score_manuscript',
						'match',
						'pattern' => '/^[0-9]+$/i',
						'message' => 'Debe introducir solo Números' 
				),
				array (
						'score_exposure',
						'match',
						'pattern' => '/^[0-9]+$/i',
						'message' => 'Debe introducir solo Números' 
				),
				array (
						'score_product',
						'match',
						'pattern' => '/^[0-9]+$/i',
						'message' => 'Debe introducir solo Números' 
				) 
		);
	}
	public function attributeLabels() {
		return array (
				// 'teacher_id' => 'Nombre',
				// 'dni' => 'Cedula',
				'title' => 'Titulo',
				'observations' => 'Observaciones',
				'date_present' => 'Fecha de Presentación',
				'status' => 'Estatus' 
		);
	}
}