<?php

class LetterForm extends CFormModel {
    public $tutor;
    public $semester;
    public $title;
    public $matter;
    
    public function rules() {
        return array(
            array(
                'tutor, semester, title',
                'required'
            ), array('matter','safe')
        );
    }

    public function attributeLabels() {
        return array(
            'tutor' => 'Nombre',
            'semester' => 'Semestre',
            'title' => 'Titulo',
            'matter' => 'Materia'
        );
    }
    
    public function get_name($id) {
    	if ($user = User::model()->findByPk($id)) {return $user->last_name . ', ' . $user->name;}
    }

}