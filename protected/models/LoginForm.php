<?php

class LoginForm extends CFormModel {
    public $username;
    public $password;
    

    private $_identity;
    
    public function rules() {
        return array(
            array(
                'username, password',
                'required',
                'message' => 'El Campo es Requerido'
            ),
            array(
                'password',
                'authenticate'
            ),
            array(
                'username, password',
                'safe'
            )
        );
    }

    public function authenticate($attribute, $params) {
        $user = User::model()->findByAttributes(array(
            "dni" => $this->username
        ));
        if ($user && md5($this->password) == $user->password) {
            Yii::app()->user->login(new CUserIdentity($this->username, $this->password));
            Yii::app()->user->setState('id', $user->id);
            Yii::app()->user->setState('name', $user->name);
            Yii::app()->user->setState('username', $user->dni);
            Yii::app()->user->setState('type', $user->type);
            Yii::app()->user->setState('last_name', $user->last_name);
            Yii::app()->user->setState('department', $user->department);
        } else {
                $this->addError($attribute, Yii::t('base', 'Usuario/Clave Invalido'));
        }
    }

    public function attributeLabels() {
        return array(
            'username' => Yii::t('model/LoginForm', 'Usuario'),
            'password' => Yii::t('model/LoginForm', 'Contraseña')
        );
    }

    public function login(){
        if($this->_identity===null){
            $this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode===UserIdentity::ERROR_NONE){
            Yii::app()->user->login($this->_identity);
            return true;
        }
        else
            return false;
    }   
    
    
}