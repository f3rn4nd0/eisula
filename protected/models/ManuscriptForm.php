<?php

class ManuscriptForm extends CFormModel {
    public $name;
    public $dni;
    public $tutor;
    public $cotutor;
    public $title;
    public $department;
    public $semester;
    public $manuscript;
    public $jury1;
    public $jury2;
    public $jury3;
    public $score_final;
    
   // public $type;   

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array(
                'name, dni, tutor,  title, department, semester,  manuscript, jury1, jury2, jury3, score_final',
                'required'),
            array( 
                'cotutor', 
                'safe'
            )
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'name' => 'Nombre y Apellido:',
            'dni' => 'Cedula:',
            'tutor' => 'Profesor Tutor:',
            'cotutor' => 'Profesor CoTutor:', 
            'department' => 'Departamento de Adscripción',
            'semester' => 'Semestre:',
            'manuscript' => 'Manuscrito del Proyecto:',
            'jury1' => 'Jurado Principal',
            'jury2' => 'Jurado Principal',
            'jury3' => 'Jurado Suplente', 
            'score_final' => 'Calificación Final'
            
        );
    }

}
