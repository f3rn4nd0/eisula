<?php

class Password extends CFormModel {
    public $dni;
    public $token;
    public $password;
    public $password1;

    public function rules() {
        return array(
            array(
                'dni, token, password, password1',
                'required'
            ),
            array(
                'dni',
                'authenticate'
            ),
            array(
                'password1',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'La contraseña no coincide'
            )
        );
    }

    public function authenticate($attribute, $params) {
        if (! User::model()->exists("dni='{$this->dni}' AND token='{$this->token}'")) {
            $this->addError($attribute, Yii::t('base', 'Usuario/Token Invalido'));
        }
    }

    public function save() {
        if ($this->validate()) {
            $user = User::model()->findByAttributes(
                array(
                    'dni' => $this->dni,
                    'token' => $this->token
                ));
            
            $user->password = md5($this->password);
            $user->token = '';
            $user->update();
            return true;
        }
        return false;
    }   
    

}