<?php

class ProfileForm extends User {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array(
                'email',
                'required'
            ),
            array(
                'email',
                'email'
            ),
            array(
                'email',
                'length',
                'max' => 50
            ),
            array(
                'id, dni, name, last_name, email, password, department, type, token',
                'safe',
                'on' => 'search'
            )
        );
    }

    public function save() {
        if ($this->validate()) {
            if ($this->password) {
                $this->password = md5($this->password);
                $this->update();
            } else {
                $this->update(array(
                    'email'
                ));
            }
            return true;
        }
        return false;
    }

}