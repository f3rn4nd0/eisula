<?php

class ProposalForm extends CFormModel {
    public $name;
    public $dni;
    public $tutor;
    public $cotutor;
    public $adviser;
    public $agency;
    public $title;
    public $department;
    public $semester;
    public $annexes;
    // public $type;
    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array(
                'name, dni, tutor,  title, department, semester,  annexes',
                'required'
            ),
            array(
                'cotutor',
                'safe'
            )
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'name' => 'Nombre y Apellido:',
            'dni' => 'Cedula:',
            'tutor' => 'Profesor Tutor:',
            'adviser' => 'Asesor:',
            'agency' => 'Organismo:',
            'title' => 'Titulo:',
            'department' => 'Departamento de Adscripción',
            'semester' => 'Semestre:',
            'annexes' => 'Propuesta:'
        );
    }

}
