<?php

class ProposaldirForm extends CFormModel {
    public $name;
    public $dni;
    public $tutor;
    public $cotutor;
    public $title;
    public $department;
    public $semester;
    public $obsdirection;
    // public $type;
    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array(
                'name, dni, tutor,  title, department, semester,  obsdirection',
                'required'
            ),
            array(
                'cotutor',
                'safe'
            )
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'name' => 'Apellido y Nombre:',
            'dni' => 'Cedula:',
            'tutor' => 'Profesor Tutor:',
            'cotutor' => 'Profesor CoTutor:',
            'title' => 'Titulo:',
            'department' => 'Departamento de Adscripción',
            'semester' => 'Semestre:',
            'obsdirection' => 'Observaciones de La propuesta'
        );
    }

}
