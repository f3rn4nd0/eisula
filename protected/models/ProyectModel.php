<?php

class ProyectModel extends Proyect {
    public $tutor;
    public $cotutor;
    public $jury1;
    public $jury2;
    public $jury3;
    public $porcentage;
    public $score_final;
    public $obsdepartment;
    public $obsdirection;
    public static $list_status = array(
        '0' => 'En espera de Aceptación del Tutor',
        '1' => 'Aceptado Por Tutor',
        '2' => 'Rechazado Por Tutor',
        '3' => 'Discusión de Propuesta',
        '4' => 'Propuesta Aprobada por Depto',
        '5' => 'Propuesta Rechazada por Depto',
        '6' => 'Propuesta Aprobada por La Dirección',
        '7' => 'Propuesta Rechazada por La Dirección',
        '8' => 'Por Presentar Avance',
        '9' => 'Por Solicitar Prórroga',
        '10' => 'Proyecto en Prórroga',
        '11' => 'Poyecto Por Presentar',
        '12' => 'Proyecto Presentado',
        '13' => 'Proyecto Finalizado'
    );
    public static $list_department = array(
        '1' => 'Control y Automatización',
        '2' => 'Investigación de Operaciones',
        '3' => 'Sistemas Computacionales',
        '4' => 'Dirección de la Escuela'
    );
    public static $list_department1 = array(
        '1' => 'Control y Automatización',
        '2' => 'Investigación de Operaciones',
        '3' => 'Sistemas Computacionales'
    );
    public static $list_type = array(
        '0' => 'Administrador',
        '1' => 'Estudiante',
        '2' => 'Profesor',
        '3' => 'Personal Administrativo',
        '4' => 'Dirección de la Escuela'
    );

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get_type() {
        if (isset(self::$list_type[$this->type])) {return self::$list_type[$this->type];}
    }

    public function get_status() {
        if (isset(self::$list_status[$this->status])) {return self::$list_status[$this->status];}
    }

    public function get_department() {
        if (isset(self::$list_department[$this->department])) {return self::$list_department[$this->department];}
        if (isset(self::$list_department1[$this->department])) {return self::$list_department1[$this->department];}
    }

    public function get_name($id) {
        if ($user = User::model()->findByPk($id)) {return $user->name . ' ' . $user->last_name;}
    }

    public function get_dni($id) {
        if ($user = User::model()->findByPk($id)) {return $user->dni;}
    }

    public function get_email($id) {
        if ($user = User::model()->findByPk($id)) {return $user->email;}
    }

    public function search() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.priority', $this->priority, true);
        $criteria->compare('t.date_present', $this->date_present, true);
        $criteria->compare('t.semester', $this->semester, true);
        $criteria->compare('t.status', $this->status, true);
        switch (Yii::app()->user->getState("type")) {
            case '0':
                $criteria->join = 'LEFT JOIN user u ON u.id=t.student_id';
                $criteria->compare('CONCAT(u.name, " ", u.last_name)', $this->student_id, true);
                $criteria->compare('t.department', $this->department, true);
            break;
            case '1':
                $criteria->compare('t.student_id', Yii::app()->user->id);
                $criteria->compare('t.department', $this->department, true);
//                 $criteria->addInCondition($column, $values)
            break;
            case '2':
                $criteria->addCondition('t.tutor=' . Yii::app()->user->id);
                $criteria->addCondition('t.cotutor=' . Yii::app()->user->id, 'OR');
                $criteria->addCondition('t.jury1=' . Yii::app()->user->id, 'OR');
                $criteria->addCondition('t.jury2=' . Yii::app()->user->id, 'OR');
                $criteria->addCondition('t.jury3=' . Yii::app()->user->id, 'OR');
                $criteria->compare('t.department', $this->department, true);
            break;
            case '3':
                $criteria->compare('t.department', Yii::app()->user->getState("department"));
            break;
            case '4':
                $criteria->join = 'LEFT JOIN user u ON u.id=t.student_id';
                $criteria->compare('CONCAT(u.name, " ", u.last_name)', $this->student_id, true);
                $criteria->compare('t.department', $this->department, true);
            default:
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria
        ));
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Título',
            'priority' => 'Materia',
            'date_present' => 'Fecha de Presentación',
            'semester' => 'Semestre',
            'department' => 'Departamento',
            'status' => 'Estatus',
            'student_id' => 'Nombre del Estudiante',
            'type' => 'Tipo de Usuario',
            'tutor' => 'Profesor Tutor',
            'cotutor' => 'Profesor Cotutor',
            'obsdepartment' => 'Observaciones Departamento',
            'obsdirection' => 'Observaciones Dirección',
            'jury1' => 'Jurado 1',
            'jury2' => 'Jurado 2',
            'jury3' => 'Jurado Suplente',
            'observations' => 'Observaciones',
            'date_present_advance' => 'Fecha de Presentación de Avance',
            'proposal' => 'Propuesta',
            'porcentage' => 'Porcentaje',
            'score_final' => 'Calificación Final',
            'manuscript' => 'Manuscrito Final'
        );
    }
    public function rules(){
        return array(
            array(
                'tutor, cotutor, jury1, jury2, jury3',
                'unique'
             )
        );
    }

}
