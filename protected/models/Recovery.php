<?php

class Recovery extends CFormModel {
    public $dni;
    public $email;
    public $password;
    private $_identity;

    public function rules() {
        return array(
            array(
                'email, dni',
                'required',
                'message' => 'El Campo es Requerido'
            ),
            array(
                'dni',
                'match',
                'pattern' => '/^[0-9]+$/i',
                'message' => 'Debe introducir solo Números'
            ),
            array(
                'email',
                'email',
                'message' => 'Formato de email Incorrecto'
            ),
            array(
                'dni',
                'authenticate'
            )
        );
    }

    public function authenticate($attribute, $params) {
        if ($user = User::model()->findByAttributes(
            array(
                "dni" => $this->dni,
                "email" => $this->email
            ))) {
            $password_en = $user->token;
            //$user->password = md5($user->password);
            //$user->save(md5($user->password));
            Yii::app()->controller->widget('ext.easy-mail.Mail', 
                array(
                    'view' => 'recoveryView',
                    'params' => array(
                        'to' => array(
                            $user->email => "{$user->name}, {$user->last_name}"
                        ),
                        'content' => array(
                            'dni' => $user->dni,
                            'email' => $user->email,
                            'token' => $password_en
                        ),
                        'subject' => 'Reinicio de Contraseña'
                    )
                ));
        } else {
            $this->addError($attribute, Yii::t('base', 'Usuario/Correo No Registrado'));
        }
    }

    public function attributeLabels() {
        return array(
            'dni' => Yii::t('model/Recovery', 'Usuario'),
            'email' => Yii::t('model/Recovery', 'Correo Electrónico')
        );
    }

}  