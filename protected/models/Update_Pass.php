<?php

class Update_Pass extends CFormModel {
    public $dni;
    public $password;
    public $password1;

    public function rules() {
        return array(
            array(
                'password, password1',
                'required',
            ),
            array(
                'password1',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'La contraseña no coincide'
            )
        );
    }

    public function save() {
        if ($this->validate()) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $user->password = md5($this->password);
            $user->update();
            return true;
        }
        return false;
    }   
    

}