<?php

class UserModel extends User {
    public static $list_department = array(
        '1' => 'Control y Automatización',
        '2' => 'Investigación de Operaciones',
        '3' => 'Sistemas Computacionales',
        '4' => 'Dirección de la Escuela',
        '5' => 'No Pertenece a la EISULA'
    );
    public static $list_type = array(
        '0' => 'Administrador',
        '1' => 'Estudiante',
        '2' => 'Profesor',
        '3' => 'Personal Administrativo',
        '4' => 'Dirección de la Escuela'
    );

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get_department() {
        if (isset(self::$list_department[$this->department])) {return self::$list_department[$this->department];}
    }

    public function get_type() {
        if (isset(self::$list_type[$this->type])) {return self::$list_type[$this->type];}
    }
    public $verifyCode;

    public function rules() {
        return array(
            array(
                'dni, name, last_name, email, password, department, type',
                'required'
            ),
//             array(
//                 'deparment, type',
//                 'save'
//             ),
            array(
                'email, dni',
                'unique'
            ),
            array(
                'email',
                'email'
            )
        // array(
        // 'verifyCode',
        // 'captcha',
        // 'allowEmpty' => ! CCaptcha::checkRequirements()
        // )
                );
    }

    public function attributeLabels() {
        return array(
            'dni' => 'Cédula',
            'name' => 'Nombres',
            'last_name' => 'Apellidos',
            'email' => 'Corrreo Electrónico',
            'password' => 'Contraseña',
            'department' => 'Departamento',
            'ext' => 'Currículum',
            'verifyCode' => 'Código de Verificación',
            'type' => 'Tipo de Usuario'
        );
    }

    public function save() {
        $this->password = md5($this->password);
        return parent::save();
    }

    public function search() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.dni', $this->dni, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.last_name', $this->last_name, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.password', $this->password, true);
        switch (Yii::app()->user->getState("type")) {
            case '0':
                $criteria->compare('t.department', $this->department);
            break;
            case '1':
            break;
            case '2':
            break;
            case '3':
                $criteria->compare('t.department', Yii::app()->user->getState("department"));
            break;
            case '4':
                $criteria->compare('t.department', $this->department);
            break;
        }
        $criteria->compare('t.type', $this->type, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria
        ));
    }

}
