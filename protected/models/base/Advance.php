<?php

/**
 * This is the model class for table "advance".
 *
 * The followings are the available columns in table 'advance':
 * @property integer $id
 * @property string $date_present
 * @property string $porcentage
 * @property string $file
 * @property string $acta
 * @property integer $proyect_id
 *
 * The followings are the available model relations:
 * @property Proyect $proyect
 */
class Advance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_present, porcentage, file, acta, proyect_id', 'required'),
			array('proyect_id', 'numerical', 'integerOnly'=>true),
			array('porcentage', 'length', 'max'=>30),
			array('file, acta', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_present, porcentage, file, acta, proyect_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proyect' => array(self::BELONGS_TO, 'Proyect', 'proyect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_present' => 'Fecha de Presentación',
			'porcentage' => 'Porcentaje de Avance',
			'file' => 'File',
			'acta' => 'Acta',
			'proyect_id' => 'Proyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_present',$this->date_present,true);
		$criteria->compare('porcentage',$this->porcentage,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('acta',$this->acta,true);
		$criteria->compare('proyect_id',$this->proyect_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Advance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
