<?php

/**
 * This is the model class for table "proyect".
 *
 * The followings are the available columns in table 'proyect':
 * @property integer $id
 * @property string $title
 * @property string $priority
 * @property string $date_present
 * @property string $semester
 * @property string $department
 * @property string $status
 * @property integer $student_id
 * @property string $agency
 * @property integer $tutor
 * @property integer $cotutor
 * @property integer $jury1
 * @property integer $jury2
 * @property integer $jury3
 * @property string $date_present_advance
 * @property string $proposal
 * @property string $porcentage
 * @property string $observations
 * @property integer $score_final
 * @property string $manuscript
 * @property string $obsdepartment
 * @property string $obsdirection
 * @property integer $score_tutor
 * @property integer $score_manuscript
 * @property integer $score_exposure
 * @property integer $score_product
 *
 * The followings are the available model relations:
 * @property Advance[] $advances
 * @property Document[] $documents
 * @property Student $student
 * @property Teacher $tutor0
 * @property Teacher $cotutor0
 * @property Teacher $jury10
 * @property Teacher $jury20
 * @property Teacher $jury30
 * @property Teacher[] $teachers

 */
class Proyect extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proyect';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, semester, status, student_id, tutor', 'required'),
			array('student_id, tutor, cotutor, jury1, jury2, jury3, score_final, score_tutor, score_manuscript, score_exposure, score_product', 'numerical', 'integerOnly'=>true),
			array('title, proposal, observations, manuscript, obsdepartment, obsdirection', 'length', 'max'=>255),
			array('priority, agency', 'length', 'max'=>20),
			array('semester, status', 'length', 'max'=>10),
			array('department', 'length', 'max'=>1),
			array('porcentage', 'length', 'max'=>30),
			array('date_present, date_present_advance', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, priority, date_present, semester, department, status, student_id, agency, tutor, cotutor, jury1, jury2, jury3, date_present_advance, proposal, porcentage, observations, score_final, manuscript, obsdepartment, obsdirection, score_tutor, score_manuscript, score_exposure, score_product', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'advances' => array(self::HAS_MANY, 'Advance', 'proyect_id'),
			'documents' => array(self::HAS_MANY, 'Document', 'proyect_id'),
			'student' => array(self::BELONGS_TO, 'Student', 'student_id'),
			'tutor0' => array(self::BELONGS_TO, 'Teacher', 'tutor'),
			'cotutor0' => array(self::BELONGS_TO, 'Teacher', 'cotutor'),
			'jury10' => array(self::BELONGS_TO, 'Teacher', 'jury1'),
			'jury20' => array(self::BELONGS_TO, 'Teacher', 'jury2'),
			'jury30' => array(self::BELONGS_TO, 'Teacher', 'jury3'),
		    'teachers' => array(self::MANY_MANY, 'Teacher', 'teacher_proyect(proyect_id, teacher_id)'),
		    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'priority' => 'Priority',
			'date_present' => 'Date Present',
			'semester' => 'Semester',
			'department' => 'Department',
			'status' => 'Status',
			'student_id' => 'Student',
			'agency' => 'Agency',
			'tutor' => 'Tutor',
			'cotutor' => 'Cotutor',
			'jury1' => 'Jury1',
			'jury2' => 'Jury2',
			'jury3' => 'Jury3',
			'date_present_advance' => 'Date Present Advance',
			'proposal' => 'Proposal',
			'porcentage' => 'Porcentage',
			'observations' => 'Observations',
			'score_final' => 'Score Final',
			'manuscript' => 'Manuscript',
			'obsdepartment' => 'Obsdepartment',
			'obsdirection' => 'Obsdirection',
			'score_tutor' => 'Score Tutor',
			'score_manuscript' => 'Score Manuscript',
			'score_exposure' => 'Score Exposure',
			'score_product' => 'Score Product',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('date_present',$this->date_present,true);
		$criteria->compare('semester',$this->semester,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('agency',$this->agency,true);
		$criteria->compare('tutor',$this->tutor);
		$criteria->compare('cotutor',$this->cotutor);
		$criteria->compare('jury1',$this->jury1);
		$criteria->compare('jury2',$this->jury2);
		$criteria->compare('jury3',$this->jury3);
		$criteria->compare('date_present_advance',$this->date_present_advance,true);
		$criteria->compare('proposal',$this->proposal,true);
		$criteria->compare('porcentage',$this->porcentage,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('score_final',$this->score_final);
		$criteria->compare('manuscript',$this->manuscript,true);
		$criteria->compare('obsdepartment',$this->obsdepartment,true);
		$criteria->compare('obsdirection',$this->obsdirection,true);
		$criteria->compare('score_tutor',$this->score_tutor);
		$criteria->compare('score_manuscript',$this->score_manuscript);
		$criteria->compare('score_exposure',$this->score_exposure);
		$criteria->compare('score_product',$this->score_product);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proyect the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
