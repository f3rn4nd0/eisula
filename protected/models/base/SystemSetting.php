<?php

/**
 * This is the model class for table "system_setting".
 *
 * The followings are the available columns in table 'system_setting':
 * @property integer $id
 * @property integer $jefe_depto_control
 * @property integer $jefe_depto_computacion
 * @property integer $jefe_depto_investigacion
 * @property integer $secrt_depto_control
 * @property integer $secrt_depto_investigacion
 * @property integer $secrt_depto_computacion
 * @property string $semester
 * @property string $next_semester
 * @property integer $chief_depto_control
 * @property integer $chief_depto_computacion
 * @property integer $chief_depto_investigacion
 *
 * The followings are the available model relations:
 * @property User $secrtDeptoComputacion
 * @property User $secrtDeptoControl
 * @property User $secrtDeptoInvestigacion
 * @property User $jefeDeptoComputacion
 * @property User $jefeDeptoInvestigacion
 * @property User $jefeDeptoControl
 */
class SystemSetting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'system_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jefe_depto_control, jefe_depto_computacion, jefe_depto_investigacion, secrt_depto_control, secrt_depto_investigacion, secrt_depto_computacion, chief_depto_control, chief_depto_computacion, chief_depto_investigacion', 'numerical', 'integerOnly'=>true),
			array('semester, next_semester', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, jefe_depto_control, jefe_depto_computacion, jefe_depto_investigacion, secrt_depto_control, secrt_depto_investigacion, secrt_depto_computacion, semester, next_semester, chief_depto_control, chief_depto_computacion, chief_depto_investigacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'secrtDeptoComputacion' => array(self::BELONGS_TO, 'User', 'secrt_depto_computacion'),
			'secrtDeptoControl' => array(self::BELONGS_TO, 'User', 'secrt_depto_control'),
			'secrtDeptoInvestigacion' => array(self::BELONGS_TO, 'User', 'secrt_depto_investigacion'),
			'jefeDeptoComputacion' => array(self::BELONGS_TO, 'User', 'jefe_depto_computacion'),
			'jefeDeptoInvestigacion' => array(self::BELONGS_TO, 'User', 'jefe_depto_investigacion'),
			'jefeDeptoControl' => array(self::BELONGS_TO, 'User', 'jefe_depto_control'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jefe_depto_control' => 'Jefe Depto Control',
			'jefe_depto_computacion' => 'Jefe Depto Computacion',
			'jefe_depto_investigacion' => 'Jefe Depto Investigacion',
			'secrt_depto_control' => 'Secrt Depto Control',
			'secrt_depto_investigacion' => 'Secrt Depto Investigacion',
			'secrt_depto_computacion' => 'Secrt Depto Computacion',
			'semester' => 'Semestre Actual',
			'next_semester' => 'Próximo Semestre',
			'chief_depto_control' => 'Chief Depto Control',
			'chief_depto_computacion' => 'Chief Depto Computacion',
			'chief_depto_investigacion' => 'Chief Depto Investigacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jefe_depto_control',$this->jefe_depto_control);
		$criteria->compare('jefe_depto_computacion',$this->jefe_depto_computacion);
		$criteria->compare('jefe_depto_investigacion',$this->jefe_depto_investigacion);
		$criteria->compare('secrt_depto_control',$this->secrt_depto_control);
		$criteria->compare('secrt_depto_investigacion',$this->secrt_depto_investigacion);
		$criteria->compare('secrt_depto_computacion',$this->secrt_depto_computacion);
		$criteria->compare('semester',$this->semester,true);
		$criteria->compare('next_semester',$this->next_semester,true);
		$criteria->compare('chief_depto_control',$this->chief_depto_control);
		$criteria->compare('chief_depto_computacion',$this->chief_depto_computacion);
		$criteria->compare('chief_depto_investigacion',$this->chief_depto_investigacion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SystemSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
