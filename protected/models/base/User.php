<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $dni
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $department
 * @property string $type
 * @property string $token
 *
 * The followings are the available model relations:
 * @property Student $student
 * @property SystemSetting[] $systemSettings
 * @property SystemSetting[] $systemSettings1
 * @property SystemSetting[] $systemSettings2
 * @property SystemSetting[] $systemSettings3
 * @property SystemSetting[] $systemSettings4
 * @property SystemSetting[] $systemSettings5
 * @property Teacher $teacher
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dni, name, last_name, email, password, department, type', 'required'),
			array('dni', 'length', 'max'=>10),
			array('name, last_name, email', 'length', 'max'=>50),
			array('password, token', 'length', 'max'=>32),
			array('department, type', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dni, name, last_name, email, password, department, type, token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'student' => array(self::HAS_ONE, 'Student', 'id'),
			'systemSettings' => array(self::HAS_MANY, 'SystemSetting', 'secrt_depto_computacion'),
			'systemSettings1' => array(self::HAS_MANY, 'SystemSetting', 'secrt_depto_control'),
			'systemSettings2' => array(self::HAS_MANY, 'SystemSetting', 'secrt_depto_investigacion'),
			'systemSettings3' => array(self::HAS_MANY, 'SystemSetting', 'jefe_depto_computacion'),
			'systemSettings4' => array(self::HAS_MANY, 'SystemSetting', 'jefe_depto_investigacion'),
			'systemSettings5' => array(self::HAS_MANY, 'SystemSetting', 'jefe_depto_control'),
			'teacher' => array(self::HAS_ONE, 'Teacher', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dni' => 'Dni',
			'name' => 'Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'password' => 'Password',
			'department' => 'Department',
			'type' => 'Type',
			'token' => 'Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dni',$this->dni,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('token',$this->token,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
