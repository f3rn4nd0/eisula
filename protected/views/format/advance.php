<?php
/* @var $this FormatController */
/* @var $model AdvanceForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Avance';
?>


<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'advance-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<?php echo $form->errorSummary($model); ?>
<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
<h1 class="text-center">PROYECTO DE GRADO</h1>
<h2 class="text-center">PRESENTACIÓN DE AVANCE</h2>
<br />

<p class="text-justify">
	<b style="color: transparent;">_____</b>

Titulo del Proyecto de Grado: <?php echo $proyect->title?>.
 Bachiller:
<b><?php echo $student->name . ' ' . $student->last_name; ?></b> CI: <b><?php echo $student->dni; ?></b>
	Los Sucritos miembros del Jurado notifican que la presentación de
	avance del Proyecto de Grado se realizo en fecha <b>
	<?php  echo preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', date('Y-m-d'))); ?></b>
	<br> <br>
<div class="form-group">
	Porcentaje:
	<?php echo CHtml::activeTextField($model, 'porcentage')?> %
	
	</div>

Observaciones:
<div class="form-group">
	      <?php
    echo $form->textArea($model, 'observations', 
        array(
            'form-groups' => 3,
            'cols' => 50
        ), array(
            'class' => 'form-control'
        ));
    ?>
		      <?php echo $form->error($model,'observations'); ?>
	   </div>

<br>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->tutor);  ?></strong> <br />
	<strong>Nombre Tutor.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->cotutor);  ?></strong>
	<br /> <strong>Nombre Cotutor.</strong>
</div>


<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury1);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury2);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>
<br />
<br />
<br />
<br />
<div class="col-xs-12 text-center">
</div>
<div class="col-xs-12 text-center">    
  
    <?php
    
echo "Mérida, ";
    $this->widget('zii.widgets.jui.CJuiDatePicker', 
        array(
            'model' => $model,
            'attribute' => 'date_present_advance',
            'language' => 'es',
            'options' => array(
                'dateFormat' => 'yy-mm-dd'
            ),
            'htmlOptions' => array(
                'size' => '10', // textField size
                'maxlength' => '10' // textField maxlength
                        )
        ));
    ?>
</div>
<br>
<br>
<br>
<center>
	<div class="form-group buttons noPrint">
		<?php echo CHtml::submitButton('Registrar Avance',array('class'=>'btn btn-medium btn-primary'));?>
	</div>


</center>
<!-- form -->
<?php $this->endWidget(); ?>    