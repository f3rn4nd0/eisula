<?php
/* @var $this FormatController */
/* @var $model AdvanceForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Avance';
?>


<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'advance-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<?php echo $form->errorSummary($model); ?>
<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
<h1 class="text-center">PROYECTO DE GRADO</h1>
<h2 class="text-center">PRESENTACIÓN DE AVANCE</h2>
<br />

<p class="text-justify">
	<b style="color: transparent;">_____</b>

Titulo del Proyecto de Grado: <?php echo $model->title?>.
 Bachiller:
<b><?php echo $model->get_name($model->student_id); ?></b> CI: <b>
<?php echo $model->get_dni($model->student_id);?></b> Los Sucritos
	miembros del Jurado notifican que la presentación de avance del
	Proyecto de Grado se realizo en fecha <b>
	<?php  echo preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', date('Y-m-d'))); ?></b>
	<br> <br>
<div class="form-group">
	Porcentaje:
	<?php echo $model->porcentage?> %
	
	</div>

Observaciones:
<div class="form-group">
	      <?php
    echo $model->observations?>
	   </div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $model->get_name($model->tutor);  ?></strong> <br />
	<strong>Nombre Tutor.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $model->get_name($model->cotutor);  ?></strong> <br />
	<strong>Nombre Cotutor.</strong>
</div>


<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $model->get_name($model->jury1);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $model->get_name($model->jury2);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>
<br />
<br />
<br />
<br />
<div class="col-xs-12 text-center">    
  <?php

echo "Mérida, " .
     preg_replace('/\//', ' de ', Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $model->date_present_advance));
?>
</div>
<br>
<br>
<br>
<center>
	<div class="form-group buttons noPrint">
		<?php echo CHtml::linkButton('Volver al Inicio',array('class'=>'btn btn-medium btn-primary', 'href' => '/eisula/site/index'));?>
	</div>

</center>
<!-- form -->
<?php $this->endWidget(); ?>    