<?php
/* @var $this ProyectController */
/* @var $model Proyect */
/* @var $form CActiveForm */
?>


<div class="form-group">
<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'proyect-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>
</div>
<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'title', array('class'=>"col-lg-2 control-label"));?>
		      <div class="col-lg-8">
		      <?php echo CHtml::encode($model->title); ?>
		      </div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'priority', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php echo CHtml::encode($model->priority); ?>
	   </div>
</div>


<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php echo CHtml::encode($model->semester); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
        		<?php echo CHtml::encode($model->department); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'status', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
        		<?php echo CHtml::encode($model->status); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'student_id', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
        		<?php echo CHtml::encode($model, 
        		    array(
                'name' => 'student_id',
                'value' => $model->get_name($model->student_id)
            ),student_id); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'tutor', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
        		<?php echo CHtml::encode($model->tutor); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury1', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php 
echo $form->dropDownList($model, 'jury1', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'jury1'); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury2', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php
echo $form->dropDownList($model, 'jury2', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'ajax' => array(
            'url' => $this->createUrl("findCed"),
            'type' => 'POST',
            'data' => array(
                'id' => 'js:this.value'
            ),
            'update' => '#cedula'
        ),
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'jury2'); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury3', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php
echo $form->dropDownList($model, 'jury3', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'ajax' => array(
            'url' => $this->createUrl("findCed"),
            'type' => 'POST',
            'data' => array(
                'id' => 'js:this.value'
            ),
            'update' => '#cedula'
        ),
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'jury3'); ?>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury4', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-8">
		<?php
echo $form->dropDownList($model, 'jury4', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'ajax' => array(
            'url' => $this->createUrl("findCed"),
            'type' => 'POST',
            'data' => array(
                'id' => 'js:this.value'
            ),
            'update' => '#cedula'
        ),
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'jury4'); ?>
		</div>
</div>

<div class="form-group col-xs-12 buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-medium btn-primary'));?>
</div>


<?php $this->endWidget(); ?>



<!-- form -->
