<?php
/* @var $this ProyectController */
/* @var $model Proyect */
/* @var $form CActiveForm */
?>


<div class="form-group">
<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'changetutor-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>


	<?php echo $form->errorSummary($model); ?>
</div>
<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'title', array('class'=>"col-lg-2 control-label"));?>
	      <div class="col-lg-3">
	      <div class="form-control"><?php echo $model->title; ?></div>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'priority', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->priority; ?></div>
	</div>
</div>


<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->semester; ?></div>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_department(); ?></div>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'status', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_status();?></div>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'student_id', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_name($model->student_id);?></div>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'tutor', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<?php 
echo $form->dropDownList($model, 'tutor', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'tutor'); ?>
	</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'cotutor', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<?php 
echo $form->dropDownList($model, 'cotutor', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'class' => 'form-control'
    ));
?>
		<?php echo $form->error($model,'cotutor'); ?>
	</div>
</div>


<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury1', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_name($model->jury1)?></div>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury2', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_name($model->jury2)?></div>
		</div>
</div>

<div class="form-group col-xs-12">
		<?php echo $form->labelEx($model,'jury3', array('class'=>"col-lg-2 control-label"));?>
		<div class="col-lg-3">
		<div class="form-control"><?php echo $model->get_name($model->jury3)?></div>
		</div>
</div>

<div class="form-group col-xs-12 buttons">
		
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-medium btn-primary'));?>
</div>


<?php $this->endWidget(); ?>



<!-- form -->
