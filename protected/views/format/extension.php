<?php
/* @var $this FormatController */
/* @var $model ExtensionForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Prorroga';
?>

<?php 
 $system = SystemSetting::model()->find();
 switch ($proyect->department) {
    case '1':
        $departamento = $system->jefe_depto_control;
    break;
    case '2':
        $departamento = $system->jefe_depto_investigacion;
        break;
    case '3':
        $departamento = $system->jefe_depto_computacion;
    break;
    default:
        $departamento = null;
    
}?>

<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'extension-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<?php echo $form->errorSummary($model); ?>

<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
<br>
<br>
<br>
<?php  echo "Mérida, " . preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', date('Y-m-d'))); ?>
<br>
<br>
<br>
Profesor
<br>
<?php echo $proyect->get_name($departamento);?><br>
Jefe del Departamento de <?php echo $proyect->get_department($proyect->department);?>
<br>
Demás Miembros del Consejo del Departamento de <?php echo $proyect->get_department($proyect->department);?>
<br>
Presente.-
<br>
<br>
<br>
 
Me dirijo a ustedes muy respetuosamente en la oportunidad de
solicitarles prórroga para la asignatura Proyecto de Grado Durante el
Semestre <?php echo $model->next_semester?>, el cual lleva por título:
<u><b>"<?php echo $proyect->title?>"</b></u>
, cuyo tutor es el/la
<u><b>Profesor(a) <?php echo $proyect->get_name($proyect->tutor);?></u>
</b>
, 
	adscrito(a) al departamneto de <?php echo $proyect->get_department($proyect->tutor0->id0->department)?>, con un avance aproximado de
<u><b><?php echo $proyect->porcentage?>% </u>
</b>
.
<br>
<br>
<br>
Atentamente.
<br>
<br>
<br>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->student_id);  ?></strong>
	<br /> <strong>C.I. N° <?php echo Yii::app()->user->getState('username')?></strong><br />
	<strong>Bachiller.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->tutor);  ?></strong> <br />
	<strong>V°B°. Profesor(a) Tutor.</strong>
</div>


<div class="col-xs-6 text-center"></div>

<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->cotutor);  ?></strong>
	<br /> <strong>V°B°. Profesor(a) CoTutor.</strong>
</div>
<br>
<br>
<br>
<br>

Fecha de Presentación del Avance: <?php echo preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $proyect->date_present_advance));?>
<br>
<br>
<br>
<div class="col-xs-12 text-center">
    <?php  echo "Mérida, " . preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', date('Y-m-d'))); ?>
</div>
<br />
<br>
<br>
<center>
	<div class="form-group buttons noPrint">
		<?php echo CHtml::submitButton('Solicitar Pórroga',array('class'=>'btn btn-medium btn-primary'));?>
	</div>
</center>
<!-- form -->
<?php $this->endWidget(); ?>    