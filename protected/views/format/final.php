<?php
/* @var $this FormatController */
/* @var $model FinalForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app ()->name . ' - Final';
?>

<?php
$form = $this->beginWidget ( 'CActiveForm', array (
		'id' => 'final-form',
		'enableClientValidation' => true,
		'clientOptions' => array (
				'validateOnSubmit' => true 
		),
		'htmlOptions' => array (
				'class' => "form-horizontal" 
		) 
) );
?>

<?php echo $form->errorSummary($model); ?>
<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
<br></br>

<h1 class="text-center">PROYECTO DE GRADO</h1>
<h2 class="text-center">CALIFICACIÓN FINAL</h2>
</br>

<p class="text-justify ident">
	Titulo del Proyecto de Grado:<b> <?php echo $proyect->title?></b>. <br />
	<br />
<div class="col-xs-6 text-center">
	<strong>Bachiller: <?php echo $student->name . ' ' . $student->last_name; ?></strong>
	<br />
</div>

<div class="col-xs-6 text-center">
	<strong>C.I. <?php echo $student->dni; ?></strong>
</div>
<br />
<br />
<div class="col-xs-6 text-justify">
	1) Calificación del (de la) Profesor(a) Tutor(a): <br />
</div>

<div class="col-xs-6 text-center">
	<input class="Print" id="score_tutor" name="score_tutor"
		style="width: 50px" type="number" min="00" max="20"> (30%)
</div>

<div class="col-xs-6 text-justify">
	2) Calificación del Manuscrito Final: <br />
</div>

<div class="col-xs-6 text-center">
	<input id="score_manuscript" name="score_manuscript"
		" style="width: 50px" type="number" min="0" max="20"> (30%)
</div>

<div class="col-xs-6 text-justify">
	3) Calificación de la Dsefensa Oral: <br />
</div>

<div class="col-xs-6 text-center">
	<input id="score_exposure" name="score_exposure" style="width: 50px"
		type="number" min="0" max="20"> (20%)
</div>

<div class="col-xs-6 text-justify">
	4) Calificación del Producto: <br />
</div>

<div class="col-xs-6 text-center">
	<input id="score_product" name="score_product" type="number" min="0"
		max="20" style="width: 50px"> (20%) <br> <br>
</div>




<div class="col-xs-6 text-justify">
	Calificación Final: <br />
</div>

<div class="col-xs-6 text-center">
	<label type="number" id="score_final"></label> (Puntos) <br> <br>
</div>

<br />
<br />
Los Sucritos miembros del Jurado asignan como calificación final del
Proyecto de Grado la nota de:
<br />
<br />
<div class="col-xs-6 text-center">
	<label type="text" id="nota"></label>
		<?php
		echo CHtml::ajaxLink ( '', array (
				'format/ajax_request' 
		), array (
				'update' => '#nota',
				'type' => 'onchange',
				'success' => 'function(html){ jQuery("#nota").html(html); }' 
		) );
		?>

	</div>
<label type="text" id="letras"></label>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->tutor);  ?></strong> <br />
	<strong>Nombre Tutor.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->cotutor);  ?></strong>
	<br /> <strong>Nombre Cotutor.</strong>
</div>


<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury1);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury2);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>
<br>
<br>
<br>
<p class="text-center"> 

<?php
echo "Mérida, ";
$this->widget ( 'zii.widgets.jui.CJuiDatePicker', array (
		'model' => $model,
		'attribute' => 'date_present',
		'language' => 'es',
		'options' => array (
				'dateFormat' => 'yy-mm-dd' 
		),
		'htmlOptions' => array (
				'size' => '10', // textField size
				'maxlength' => '10'  // textField maxlength
				) 
) );
?>

	<br> <br> <br>
<div class="form-group buttons" align="center">
	<div class="noPrint">
		<div class="form-group buttons"> <?php echo CHtml::submitButton('Registrar Acta Final', array('class'=>'btn btn-medium btn-primary' ));?> </div>
	</div>
	</p>
</div>
<!-- form -->
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(function() {
    $("input[id^=score]").on("change", function() {

    	$.ajax({
    		url: '<?php echo Yii::app()->createAbsoluteUrl('format/ajax_request'); ?>',
    		data: {
    			   nota1: $("#score_tutor").val()*.3,
    			   nota2: $("#score_manuscript").val()*.3,
    			   nota3: $("#score_exposure").val()*.2,
    			   nota4: $("#score_product").val()*.2,
    			   nota5: $("#score_final").val(),
    			   },
    		//dataType: "JSON",
    		success : function(data) {
    			var myObject = eval('(' + data + ')');
    			$("#nota").html(myObject.nota);
    			$("#letras").html(myObject.letras);
    			$("#score_final").html(myObject.score_final);
    	  }
        });
    });
});
</script>
