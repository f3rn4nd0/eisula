<?php
/* @var $this FormatController */
/* @var $model FinalForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Final';
?>

<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'final-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<?php echo $form->errorSummary($model); ?>
<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
<br></br>

<h1 class="text-center">PROYECTO DE GRADO</h1>
<h2 class="text-center">CALIFICACIÓN FINAL</h2>
</br>

<p class="text-justify ident">
	Titulo del Proyecto de Grado:<b> <?php echo $proyect->title?></b>. <br />
	<br />
<div class="col-xs-6 text-center">
	<strong>Bachiller: <?php echo $student->name . ' ' . $student->last_name; ?></strong>
	<br />
</div>

<div class="col-xs-6 text-center">
	<strong>C.I. <?php echo $student->dni; ?></strong>
</div>
<br />
<br />
<div class="col-xs-6 text-justify">
	1) Calificación del (de la) Profesor(a) Tutor(a): <br />
</div>

<div class="col-xs-6 text-center">
	<?php echo $proyect->score_tutor?> (30%)
</div>

<div class="col-xs-6 text-justify">
	2) Calificación del Manuscrito Final: <br />
</div>

<div class="col-xs-6 text-center">
	<?php echo $proyect->score_manuscript?> (30%)
</div>

<div class="col-xs-6 text-justify">
	3) Calificación de la Dsefensa Oral: <br />
</div>

<div class="col-xs-6 text-center">
	<?php echo $proyect->score_exposure?> (20%)
</div>

<div class="col-xs-6 text-justify">
	4) Calificación del Producto: <br />
</div>

<div class="col-xs-6 text-center">
	<?php echo $proyect->score_product?> (20%) <br> <br>
</div>




<div class="col-xs-6 text-justify">
	Calificación Final: <br />
</div>
<div class="col-xs-6 text-center">
<?php echo $proyect->score_final ?> (Puntos)
</div>
<br />
<br>

<br />
<br />
Los Sucritos miembros del Jurado asignan como calificación final del
Proyecto de Grado la nota de:
<br />
<br />
<strong>
<div class="col-xs-6 text-center">
	<label type="text" id="nota"></label>
		<?php
echo $proyect->score_final 
    
?>

	</div>
<label type="text" id="letras"></label>
<?php echo Numeric::num_to_letras($proyect->score_final, 'PUNTOS', null)?>
</strong>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->tutor);  ?></strong> <br />
	<strong>Nombre Tutor.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->cotutor);  ?></strong>
	<br /> <strong>Nombre Cotutor.</strong>
</div>


<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury1);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>

<div class="col-xs-6 text-center">
	<center>
		<br /> <br />
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo $proyect->get_name($proyect->jury2);  ?></strong> <br />
	<strong>Nombre Jurado.</strong>
</div>
<br />
<br>
<br>
<p class="text-center"> 
<br>
<br>
<br>
<?php echo "Mérida, "
    
     . preg_replace('/\//', ' de ', Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $proyect->date_present));
    ?>

	<br> <br> <br>
<div class="form-group buttons" align="center">
	<div class="noPrint">
		<div class="form-group buttons"> <?php echo CHtml::submitButton('Volver al Inicio', array('class'=>'btn btn-medium btn-primary' ));?> </div>
	</div>
	</p>
</div>
<!-- form -->
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(function() {
    $("input[id^=score]").on("change", function() {

    	$.ajax({
    		url: '<?php echo Yii::app()->createAbsoluteUrl('format/ajax_request'); ?>',
    		data: {
    			   nota: $("score_final").val(),
    			   },
    		//dataType: "JSON",
    		success : function(data) {
    			var myObject = eval('(' + data + ')');
    			$("#nota").html(myObject.nota);
    			$("#letras").html(myObject.letras);
    			$("#score_final").html(myObject.score_final);
    	  }
        });
    });
});
</script>
