<?php
/* @var $this FormatController */
/* @var $model LettertForm */
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name . ' - Carta';
?>

<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'letter-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

    <?php echo $form->errorSummary($model); ?>

<img alt="logo" width="100%"
	src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">

<h1 class="text-center">PROYECTO DE GRADO</h1>
<h2 class="text-center">CARTA DE COMPROMISO</h2>
<br />
<br />
<br />
<p class="text-justify">
	<b style="color: transparent;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b>
	Quien suscribe, Prof <strong><?php
echo $form->dropDownList($model, 'tutor', 
    CHtml::listData(
        User::model()->findAll(
            array(
                'condition' => 'type =2',
                'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
            )), 'id', 'name'), 
    array(
        'prompt' => 'Seleccione',
        'ajax' => array(
            'url' => $this->createUrl("findCed"),
            'type' => 'POST',
            'data' => array(
                'id' => 'js:this.value'
            ),
            'update' => '#cedula'
        )
    ));
?> <?php CHtml::activeId($model, 'tutor')?> </strong>, titular de la
	cédula de identidad Nº <strong id="cedula"><b
		style="color: transparent;">_____</b></strong>, mediante la presente
	me comprometo a ser el Profesor(a) Tutor(a) del Proyecto de grado del
	(de la ) Br. <strong><?php echo Yii::app()->user->getState('name').' '.Yii::app()->user->getState('last_name')?></strong>,
	titular de la cédula de identidad Nº <strong><?php echo Yii::app()->user->getState('username')?></strong>,
	el cual será desarrollado en el área temática <?php echo CHtml::activeTextField($model,'title')?>
	durante el semestre, <strong><?php echo $model->semester; ?></strong>
	mismo que se regirá de acuerdo al Reglamento de Proyectos de Grado de
	la Escuela de Ingeniería de Sistemas vigente.
</p>

<p class="text-justify">
	<b style="color: transparent;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b>
    Si el Proyecto de Grado será cursado con una última materia, indique el nombre de la misma
    <?php echo CHtml::activeTextField($model,'matter'); ?>
</p>
<br>
<br>
<br>
<br>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><?php echo Yii::app()->user->getState('name').' '.Yii::app()->user->getState('last_name'); ?></strong>
	<br /> <strong>Bachiller.</strong>
</div>
<div class="col-xs-6 text-center">
	<center>
		<div
			style="border-bottom: solid 0.2em; width: 300px; margin-bottom: 10px;"></div>
	</center>
	<strong><div id="demo"></div></strong> <br /> <strong>Tutor.</strong>
</div>

<div class="col-xs-12 text-center">
    <?php  echo "Mérida, " . preg_replace('/\//', ' de ',Yii::app()->dateFormatter->format('dd/MMMM/yyyy', date('Y-m-d'))); ?>
</div>

<br />
<br />
<br />

<div class="form-group buttons noPrint" align="center">
		<?php echo CHtml::submitButton('Registrar Carta',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<!-- form -->
<?php $this->endWidget(); ?>
		
	