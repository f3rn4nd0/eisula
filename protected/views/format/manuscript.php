<?php
/* @var $this SiteController */
/* @var $model manuscriptForm */
/* @var $form CActiveForm */

?>

<h1>Manuscrito</h1>

<?php if(Yii::app()->user->hasFlash('manuscript')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('manuscript'); ?>
</div>

<?php else: ?>
<div class="form">
<?php
    $form = $this->beginWidget('CActiveForm', 
        array(
            'id' => 'manuscript-form',
            'focus' => array(
                $model,
                'name'
            ),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true
            ),
            'htmlOptions' => array(
                'class' => "form-horizontal",
                'enctype' => 'multipart/form-data'
            )
        ));
    ?>

	<p class="note">
		Los Campos con <span class="required">*</span> son Requeridos.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo Yii::app()->user->getState('name').' '.Yii::app()->user->getState('last_name')?></div>

		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo Yii::app()->user->getState('username')?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->tutor)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cotutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->cotutor)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'title', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control" style="max-height: auto; height: auto;"><?php echo $proyect->title?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->semester?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_department()?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jury1', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->jury1)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jury2', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->jury2)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jury3', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->jury3)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'score_final', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->score_final?><span> Puntos</span></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'manuscript', array('class'=>"col-lg-2 control-label")); ?>
		<?php echo CHtml::activeFileField ($model,'manuscript');?>

</div>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->

<?php endif; ?>
	