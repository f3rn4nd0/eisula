<?php
/* @var $this SiteController */
/* @var $model ProposalForm */
/* @var $form CActiveForm */

?>

<h1>Propuesta</h1>

<?php if(Yii::app()->user->hasFlash('proposal')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('proposal'); ?>
</div>

<?php else: ?>
<div class="form">
<?php
    $form = $this->beginWidget('CActiveForm', 
        array(
            'id' => 'proposal-form',
            'focus' => array(
                $model,
                'name'
            ),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true
            ),
            'htmlOptions' => array(
                'class' => "form-horizontal",
                'enctype' => 'multipart/form-data'
            )
        ));
    ?>

	<p class="note">
		Los Campos con <span class="required">*</span> son Requeridos.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo Yii::app()->user->getState('name').' '.Yii::app()->user->getState('last_name')?></div>

		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo Yii::app()->user->getState('username')?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->tutor)?></div>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'title', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control" style="max-height: auto; height: auto;"><?php echo $proyect->title?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->semester?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cotutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<?php echo $form->dropDownList($model, 'cotutor', CHtml::listData(
                User::model()->findAll(
                    array(
                        'condition' => 'type =2',
                        'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                    )), 'id', 'name'),array('prompt' =>'Seleccione', 'class'=>'form-control'));?>
                <?php echo $form->error($model,'cotutor'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
		      <?php echo $form->dropDownList($model, 'department', 
		          array('1' => 'Control y Automatización','2' => 'Investigación de Operaciones','3' => 'Sistemas Computacionales'), array('prompt' =>'Seleccione', 'class'=>'form-control')); ?>
		      <?php echo $form->error($model,'department'); ?>
	    </div>
	    		          <span class="fa fa-question" title="Departamento Donde Inscribirá el Proyecto de Grado"></span>
	</div>



	<div class="form-group">
		<?php echo $form->labelEx($model,'adviser', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
		      <?php echo $form->textField($model,'adviser',array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'adviser'); ?>
	    </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'agency', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
		      <?php echo $form->textField($model,'agency',array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'agency'); ?>
	    </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'annexes', array('class'=>"col-lg-2 control-label")); ?>
		<?php echo CHtml::activeFileField ($model,'annexes');?>
</div>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->

<?php endif; ?>
	