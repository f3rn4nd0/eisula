<?php
/* @var $this SiteController */
/* @var $model ProposaldeptoForm */
/* @var $form CActiveForm */
?>
<h1>Correcciones a la Propuesta</h1>
<?php if(Yii::app()->user->hasFlash('proposaldepto')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('proposaldepto'); ?>
</div>
<?php else: ?>
<div class="form">
<?php
    $form = $this->beginWidget('CActiveForm', 
        array(
            'id' => 'proposaldepto-form',
            'focus' => array(
                $model,
                'name'
            ),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true
            ),
            'htmlOptions' => array(
                'class' => "form-horizontal",
                'enctype' => 'multipart/form-data'
            )
        ));
    ?>
	<p class="note">
		Los Campos con <span class="required">*</span> son Requeridos.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->student_id) ?></div>

		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_dni($proyect->student_id)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'title', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control" style="max-height: auto; height: auto;"><?php echo $proyect->title?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->tutor)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cotutor', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_name($proyect->cotutor)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->semester?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
			<div class="form-control"><?php echo $proyect->get_department($proyect->department)?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'obsdepartment', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-5">
		<?php echo $form->textArea($model,'obsdepartment',array('form-groups'=>6, 'cols'=>43),array('class'=>'form-control')); ?></div>
	</div>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->

<?php endif; ?>
	