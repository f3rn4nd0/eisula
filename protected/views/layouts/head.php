<head>
<meta charset="<?php echo Yii::app()->charset;?>">
<title><?php echo Yii::app()->name;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon"
	href="<?php echo Yii::app()->baseUrl.'/resources/img/ula.ico'; ?>">
<?php
Yii::app()->clientscript->registerCssFile(Yii::app()->baseUrl . '/resources/bootstrap/css/bootstrap.min.css')
    ->registerCssFile(Yii::app()->baseUrl . '/resources/bootstrap/css/styles.css')
    ->registerScriptFile(Yii::app()->baseUrl . '/resources/bootstrap/js/bootstrap.min.js', CClientScript::POS_END)
    ->registerCoreScript('jquery')?>


<link rel="stylesheet"
	href="<?php echo Yii::app()->baseUrl.'/resources/font-awesome/css/font-awesome.min.css'?>">
<style type="text/css">
    
@media print {
	.noPrint {
		display: none;
	}
	.Print {
		width: 100%;
	}
	
}
</style>
<style type="text/css">
.ident {
	text-indent: 2cm
}
</style>


</head>
