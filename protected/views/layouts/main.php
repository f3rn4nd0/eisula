<html lang="<?php echo Yii::app()->language;?>">
<style>
div.form-control {
	min-height: 32px;
	height: auto;
	background-color: #F0F0F0;
}
</style>
<?php require_once(dirname(__FILE__).'/head.php')?>
<body>
<?php
require_once (dirname ( __FILE__ ) . '/navbar.php')?>
<?php $this->widget('ext.notify.Notify');?>
	<div class="container-fluid">

		<div class="rt-container">
			<div class="noPrint">
				<div class="rt-grid-12 rt-alpha rt-omega">

					<div class="rt-block">
						<table style="width: 950px;" align="center" border="0">
							<tbody>
								<tr>
									<td><img style="float: left;"
										src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logosimbolo.png"; ?>"
										alt="" height="80" width="360"></td>
									<td>&nbsp;</td>
									<td><img style="float: right;"
										src="<?php echo Yii::app()->baseUrl . "/resources/img/logois.jpeg"; ?>"
										alt="" height="100" width="130"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="clear"></div>
			<div class="noPrint">
				<hr>
			</div>
			<div class="noPrint">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="/resources/bootstrap/css/styles.css">
				<style>
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
	width: 50%;
	margin: auto;
}
</style>


				<div class="container">
					<br>
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img
									src="<?php echo Yii::app()->baseUrl . "/resources/img/encabezados/encabezados_escuela.jpg"; ?>">
							</div>

							<div class="item">
								<img
									src="<?php echo Yii::app()->baseUrl . "/resources/img/encabezados/encabezados_control.jpg"; ?>">
							</div>

							<div class="item">
								<img
									src="<?php echo Yii::app()->baseUrl . "/resources/img/encabezados/encabezados_operaciones.jpg"; ?>">
							</div>

							<div class="item">
								<img
									src="<?php echo Yii::app()->baseUrl . "/resources/img/encabezados/encabezados_computacion.jpg"; ?>">
							</div>

						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" role="button"
							data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
							aria-hidden="true"></span> <span class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel"
							role="button" data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>


			</div>
		</div>
		<div class="noPrint">
			<hr>
		</div>
		<?php if (count($this->menu)):?>
	 	<div class="col-xs-2 noPrint">
		<?php require_once(dirname(__FILE__).'/navmenu.php')?>
		</div>
		<div class="col-xs-10 Print">
		<?php echo $content;?>
		</div>
		<?php else:?>
		<div class="col-xs-12">
		<?php echo $content;?>
		</div>
		<?php endif;?>
		<hr>
	</div>
	<div class="col-xs-12 noPrint">
		<?php require_once(dirname(__FILE__).'/footer.php')?>
	</div>
</body>
</html>
