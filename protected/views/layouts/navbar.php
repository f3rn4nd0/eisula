<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"> <?php echo CHtml::link('Sistema de Gestión de
				Proyectos de Grado',Yii::app()->createAbsoluteUrl('/site'), array('class'=>'navbar-link'))?></li></a>
				<li class="active"> <?php echo CHtml::link('Inicio', Yii::app()->createAbsoluteUrl('/site'),array('class'=>"navbar-link"))?></li>
				<li> <?php echo CHtml::link('Contacto', Yii::app()->createAbsoluteUrl('/site/contact'),array('class'=>"navbar-link"))?></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><?php echo CHtml::link(Yii::app()->user->getState('name').' '.Yii::app()->user->getState('last_name'),  array('class'=>"navbar-link"));?></li>
				<?php if(!Yii::app()->user->isGuest):?>
				<li><?php echo CHtml::link('Editar Perfil', Yii::app()->createAbsoluteUrl('site/profile'), array('class'=>"navbar-link")) ?></li>
				<li><?php echo CHtml::link('Salir', Yii::app()->createAbsoluteUrl('/site/logout'),array('class'=>"navbar-link"))?></li>
				<?php else: ?>
				<li><?php echo CHtml::link('Iniciar Sesión', Yii::app()->createAbsoluteUrl('/site/login'),array('class'=>"navbar-link"))?></li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" arial-expanded="false">Registro<span
						class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a tabindex="-1"> <?php echo CHtml::link('Registro Estudiantes',Yii::app()->createAbsoluteUrl('register/registerest'),array('class'=>"navbar-link")) ?> </a>
						</li>
						<li><a tabindex="-1"> <?php echo CHtml::link('Registro Profesores',Yii::app()->createAbsoluteUrl('register/registerprof'),array('class'=>"navbar-link"))?></a>
						</li>
						<li><a tabindex="-1"> <?php echo CHtml::link('Registro Personal Administrativo',Yii::app()->createAbsoluteUrl('register/registeradm'),array('class'=>"navbar-link"))?></a>
						</li>
					</ul></li>
					<?php endif;?>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
