<?php
/* @var $this ProyectController */
/* @var $model ProyectModel */
?>

<h1>Administrar Proyectos</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', 
    array(
        'id' => 'proyect-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'status',
                'value' => '$data->get_status()',
            ),
            array(
                'name' => 'student_id',
                'value' => '$data->get_name($data->student_id)'
            ),
            array(
                'name' => 'tutor',
                'value' => '$data->get_name($data->tutor)'
            ),
            'title',
            'priority',
            'semester',
            array(
                'name' => 'department',
                'value' => '$data->get_department()',
                'filter' => CHtml::activeDropDownList($model, 'department', ProyectModel::$list_department1, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{view} {update} {delete}  {reject} {rejectdir} {approveddir} {pdf} {advance} {final} {final1} {manuscript}',
                'buttons' => array(
//                     'approved' => array(
//                         'label' => '',
//                         'url' => 'Yii::app()->createAbsoluteUrl("proyect/check/$data->id")',
//                         'imageUrl' => '',
//                         'options' => array(
//                             'class' => 'fa fa-check',
//                             'title' => 'Aprobar'
//                         ),
//                         'visible' => '$data->status ==  "3"'
//                     ),
                    'reject' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("format/proposaldepto/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil',
                            'title' => 'Corregir Propuesta'
                        ),
                        'visible' => '$data->status ==  "3"'
                    ),
                    'approveddir' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("proyect/checkdir/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-check',
                            'title' => 'Aprobar'
                        ),
                        'visible' => '$data->status ==  "4" AND Yii::app()->user->getState("type")== "4"'
                    ),
                    'rejectdir' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("format/proposaldir/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil',
                            'title' => 'Rechazar Propuesta'
                        ),
                        'visible' => '$data->status ==  "4" AND Yii::app()->user->getState("type")== "4"'
                    ),
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-search',
                            'title' => 'Detalles'
                        ),
                        'visible' => ' true'
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-gavel',
                            'title' => 'Asignar Jurados'
                        ),
                        'visible' => ' $data->status == "3" AND (Yii::app()->user->getState("type")== "3")'
                    ),
                    'advance' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("format/advance/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-file-text',
                            'title' => 'Registrar Avance'
                        ),
                        'visible' => '$data->status ==  "6" AND Yii::app()->user->getState("type")== "3"'
                    ),
                    'final' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("format/final/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-graduation-cap',
                            'title' => 'Registrar Acta de Grado'
                        ),
                        'visible' => '$data->status ==  "11" OR $data->status == "9"'
                    ),
                    'final1' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl("format/final/$data->id")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-graduation-cap',
                            'title' => 'Registrar Acta de Grado'
                        ),
                        'visible' => '$data->status ==  "10"'
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-times',
                            'title' => 'Eliminar'
                        ),
                        'visible' => ' true'
                    ),
                    'pdf' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl($data->proposal)',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-download',
                            'target' => '_blank',
                            'title' => 'Descargar Propuesta',
                            'download' => true
                        ),
                        'visible' => 'true'
                    ),
                    'manuscript' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl($data->manuscript)',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-cloud-download',
                            'target' => '_blank',
                            'title' => 'Descargar Manuscrito Final',
                            'download' => true
                        ),
                        'visible' => '$data->status ==  "13"'
                    )
                ),
                'htmlOptions' => array(
                    'style' => 'width: 100px;',
                	'style' => 'Print'
                )
            )
        )
    ));