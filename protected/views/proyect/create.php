<?php
/* @var $this ProyectController */
/* @var $model Proyect */

$this->breadcrumbs=array(
	'Proyectos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Proyectos', 'url'=>array('index')),
	array('label'=>'Administrar Proyectos', 'url'=>array('admin')),
);
?>

<h1>Crear Proyecto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>