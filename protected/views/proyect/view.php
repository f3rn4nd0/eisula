<?php
/* @var $this ProyectController */
/* @var $model ProyectModel */

?>

<h1>Detalles del Proyecto </h1>

<?php
$this->widget('zii.widgets.CDetailView', 
    array(
        'data' => $model,
        'attributes' => array(
          //  'id',
            'title',
            'priority',
            'date_present',
            'semester',
            array(
                'name' => 'department',
                'value' => $model->get_department()
            ),
            array(
                'name' => 'status',
                'value' => $model->get_status()
            ),
            array(
                'name' => 'student_id',
                'value' => $model->get_name($model->student_id)
            ),
            array(
                'name' => 'tutor',
                'value' => $model->get_name($model->tutor)
            ),
            array(
                'name' => 'cotutor',
                'value' => $model->get_name($model->cotutor)
            ),

            array(
                'name' => 'jury1',
                'value' => $model->get_name($model->jury1)
            ),
            array(
                'name' => 'jury2',
                'value' => $model->get_name($model->jury2)
            ),
            array(
                'name' => 'jury3',
                'value' => $model->get_name($model->jury3)
            ),
            array(
                'name' => 'date_present_advance',
                'value' => preg_replace('/\//', ' de ', 
                    Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $model->date_present_advance))
            ),
            array('name' =>'porcentage'
            ),
            array(
                'name' => 'date_present',
                'value' => preg_replace('/\//', ' de ', 
                    Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $model->date_present))
            ),
            array('name' =>'score_final'
            ),
        )
    ));
?>
