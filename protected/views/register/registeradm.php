<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
/* @var $form TbActiveForm */
/** @var BootActiveForm $form */


$this->pageTitle=Yii::app()->name . ' Registro de Personal Administrativo';
$this->breadcrumbs=array('Registro',);?>

<h1>Registro de Personal Administrativo</h1>

<?php if(Yii::app()->user->hasFlash('registeradm')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('registeradm'); ?>
</div>

<?php else: ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registeradm-form',
	'focus'=>array($model,'dni'),
	'enableClientValidation'=>true,
	'clientOptions'=>array('validateOnSubmit'=>true,),
    'htmlOptions' => array(
            'class' => "form-horizontal"
        )
	
)); ?>
<p class="note">
	Los Campos con <span class="required">*</span> son Requeridos.
</p>


<?php echo $form->errorSummary($model); ?>


<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		 		<?php echo $form->textField($model,'dni', array('class'=>'form-control')); ?>
		 		<?php echo $form->error($model,'dni'); ?>
		 </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'name'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'last_name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'last_name', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'last_name'); ?>
		</div>
</div>



<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'email'); ?>
	    </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->passwordField($model,'password', array('class'=>'form-control'));?>
		      <?php echo $form->error($model,'password'); ?>
		</div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
  <?php echo $form->dropDownList($model, 'department', UserModel::$list_department, array('class'=>'form-control', 'prompt' => 'Selecione una Opción')); ?>
		      <?php echo $form->error($model,'department'); ?>
	   </div>
</div>



<?php if(CCaptcha::checkRequirements()): ?>
<div class="form-group">
		<?php echo $form->labelEx($model,'verifyCode',array('class'=>"col-lg-2 control-label")); ?>
        
		<div class="col-lg-3">

		       <?php echo $form->textField($model,'verifyCode', array('class'=>'form-control')); ?>
		</div>
</div>
    <?php $this->widget('CCaptcha'); ?>
	<div class="hint">Por favor Introduzca las Letras como se Muestran en
		la Imagen.</div>
	<div class="hint">Las letras no distinguen entre mayúsculas y
		minúsculas.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
<?php endif; ?>

<div class="form-group buttons">
		<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

<!-- form -->

<?php endif; ?>
