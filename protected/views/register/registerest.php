<?php
/* @var $this RegisterController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
/* @var $form TbActiveForm */
/** @var BootActiveForm $form */


$this->pageTitle=Yii::app()->name . ' Registro de Estudiantes';
$this->breadcrumbs=array('Registro',);?>

<h2>Registro de Estudiantes</h2>

<?php if(Yii::app()->user->hasFlash('registerest')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('registerest'); ?>
</div>

<?php else: ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registerest-form',
	'focus'=>array($model,'dni'),
	'enableClientValidation'=>true,
	'clientOptions'=>array('validateOnSubmit'=>true,),
    'htmlOptions' => array('class' => "form-horizontal")
	
)); ?>

<p class="note">
	Los Campos con <span class="required">*</span> son Requeridos.
</p>


<?php echo $form->errorSummary($model); ?>


<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		 		<?php echo $form->textField($model,'dni', array('class'=>'form-control')); ?>
		 		<?php echo $form->error($model,'dni'); ?>
		 </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'name'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'last_name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'last_name', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'last_name'); ?>
		</div>
</div>



<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'email'); ?>
	    </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->passwordField($model,'password', array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'password'); ?>
		</div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->dropDownList($model, 'department', array('1' => 'Control y Automatización','2' => 'Investigación de Operaciones','3' => 'Sistemas Computacionales'), array('class'=>'form-control', 'prompt' => 'Selecione una Opción')); ?>
		      <?php echo $form->error($model,'department'); ?>
	   </div>
</div>



<?php if(CCaptcha::checkRequirements()): ?>
<div class="form-group">
		<?php echo $form->labelEx($model,'verifyCode',array('class'=>"col-lg-2 control-label")); ?>
        
		<div class="col-lg-3">

		       <?php echo $form->textField($model,'verifyCode', array('class'=>'form-control')); ?>
		</div>
</div>
<?php $this->widget('CCaptcha'); ?>
<div class="hint">Por favor Introduzca las Letras como se Muestran en la
	Imagen.</div>
<div class="hint">Las letras no distinguen entre mayúsculas y minúsculas.</div>
<?php echo $form->error($model,'verifyCode'); ?>
<?php endif; ?>

<div class="form-group buttons">
		<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

<!-- form -->

<?php endif; ?>
