<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Contact Us';
$this->breadcrumbs = array('Contact');
?>

<h1>Contactenos</h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>


Si tiene consultas u otras preguntas, por favor complete el siguiente formulario para contactar con nosotros. Gracias.


<div class="form">

<?php
    $form = $this->beginWidget('CActiveForm', 
        array(
            'id' => 'contact-form',
            'focus' => array(
                $model,
                'name'
            ),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true
            ),
            'htmlOptions' => array(
                'class' => "form-horizontal"
            )
        ));
    ?>

	<p class="note">
		Los Campos con <span class="required">*</span> son Requeridos.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'name'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'email'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'subject', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'subject',array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'subject'); ?>
	    </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'body', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textArea($model,'body',array('form-groups'=>6, 'cols'=>31),array('class'=>'form-control')); ?>
		      <?php echo $form->error($model,'body'); ?>
	   </div>
	</div>

<?php if(CCaptcha::checkRequirements()): ?>
<div class="form-group">
		<?php echo $form->labelEx($model,'verifyCode',array('class'=>"col-lg-2 control-label")); ?>
        
		<div class="col-lg-3">

		       <?php echo $form->textField($model,'verifyCode', array('class'=>'form-control')); ?>
		</div>
	</div>
    <?php $this->widget('CCaptcha'); ?>
	<div class="hint">Por favor Introduzca las Letras como se Muestran en
		la Imagen.</div>
	<div class="hint">Las letras no distinguen entre mayúsculas y
		minúsculas.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
<?php endif; ?>

<div class="form-group buttons">
		<?php echo CHtml::submitButton('Enviar',array('class'=>'btn btn-medium btn-primary'));?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->

<?php endif; ?>
	