<?php
/*
 * "varCExistValidator /* @var $this SiteController
 */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle = Yii::app()->name;
?>
<div class="hero-unit">
	<h1>Bienvenid@ Bachiller</h1>

</div>
<!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', 
    array(
        'id' => 'proyect-grid',
        'dataProvider' => $model->search(),
        // 'filter' => $model,
        'columns' => array(
            array(
                'name' => 'status',
                'value' => '$data->get_status()',
                'filter' => CHtml::activeDropDownList($model, 'status', ProyectModel::$list_status, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'name' => 'tutor',
                'value' => '$data->get_name($data->tutor)'
            ),
            'title',
            'priority',
            'semester',
            array(
                'name' => 'department',
                'value' => '$data->get_department()'
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => ' {view} {update} {extension} {manuscript} {manuscript1} {changetitle} {changetutor}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'url' => 'Yii::app()->controller->createAbsoluteUrl("site/viewDetailsST/{$data->id}")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-search',
                            'title' => 'Detalles'
                        ),
                        'visible' => 'true'
                    ),
                	'changetitle' => array(
						'label' => '',
                			'url' => 'Yii::app()->controller->createAbsoluteUrl("format/changetitle/{$data->id}")',
                			'imageUrl' => '',
                			'options' => array(
                					'class' => 'fa fa-pencil-square',
                					'title' => 'Cambio Titulo'
                			),
                			'visible' => '$data->status == "5"'
                					),
                		
                    'changetutor' => array(
                        'label' => '',
                        'url' => 'Yii::app()->controller->createAbsoluteUrl("format/changetutor/{$data->id}")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil',
                            'title' => 'Cambio Tutor'
                        ),
                        'visible' => '$data->status == "7"'
                    ),
                		
                    'update' => array(
                        'label' => '',
                        'url' => 'Yii::app()->controller->createAbsoluteUrl("format/proposal/{$data->id}")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil-square-o',
                            'title' => 'Registrar Propuesta'
                        ),
                        'visible' => '$data->status=="1" OR $data-> status == "5" OR $data-> status == "7" '
                    ),
                    'extension' => array(
                        'label' => '',
                        'url' => 'Yii::app()->controller->createAbsoluteUrl("format/extension")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-clock-o',
                            'title' => 'Solicitar Prórroga'
                        ),
                        'visible' => '$data->status=="9"'
                    ),
                    
                    'manuscript' => array(
                        'label' => '',
                        'url' => 'Yii::app()->controller->createAbsoluteUrl("format/manuscript/{$data->id}")',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-cloud-upload',
                            'title' => 'Subir Proyecto'
                        ),
                        'click' => '...',
                        'visible' => '$data->status ==  "12"'
                    ),
                    'manuscript1' => array(
                        'label' => '', // Text label of the button.
                        'url' => 'Yii::app()->createAbsoluteUrl($data->manuscript)', // A PHP expression for generating
                                                                                     // the URL of the button.
                        'imageUrl' => '', // Image URL of the button.
                        'options' => array(
                            'class' => 'fa fa-cloud-download',
                            'target' => '_blank',
                            'title' => 'Descargar Manuscrito Final',
                            'download' => true
                        ), // HTML options for the button tag.
                        'click' => '...', // A JS function to be invoked when the button is clicked.
                        'visible' => '$data->status ==  "13"' // A PHP expression for determining whether the button is
                                                          // visible.
                                        )
                )
            )
        )
    ));
?>