<?php
/* @var $this SiteController */
/* @var $dataProvider CActiveDataProvider */
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = array(
    'Proyectos' => array(
        'index'
    ),
    'Administrar'
);
?>


<div class="hero-unit">
	<h1>Bienvenid@ Profesor</h1>
</div>
<!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', 
    array(
        'id' => 'proyect-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'Estatus',
                'value' => '$data->get_status()',
                'filter' => CHtml::activeDropDownList($model, 'status', ProyectModel::$list_status, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'name' => 'student_id',
                'value' => '$data->get_name($data->student_id)'
            ),
            'title',
//             'priority',
            array(
                'name' => 'tutor',
                'value' => '$data->get_name($data->tutor)'
            ),
            array(
                'name' => 'cotutor',
                'value' => '$data->get_name($data->cotutor)'
            ),
            array(
                'name' => 'jury1',
                'value' => '$data->get_name($data->jury1)'
            ),
            array(
                'name' => 'jury2',
                'value' => '$data->get_name($data->jury2)'
            ),
            array(
                'name' => 'jury3',
                'value' => '$data->get_name($data->jury3)'
            ),
            'semester',
            array(
                'name' => 'Departamento',
                'value' => '$data->get_department()',
                'filter' => CHtml::activeDropDownList($model, 'department', ProyectModel::$list_department1, 
                    array(
                        'prompt' => ''
                    ))
            ),
				
		/*
		'student_id',
		*/
		array(
                'class' => 'CButtonColumn',
                'viewButtonUrl' => 'Yii::app()->controller->createAbsoluteUrl("site/viewDetailsT/{$data->id}")',
                'updateButtonUrl' => 'Yii::app()->controller->createAbsoluteUrl("site/viewDetails/{$data->id}")',
                'template' => '{view} {update} {pdf} {manuscript}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-search',
                            'title' => 'Detalles'
                        ),
                        'visible' => ' true'
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil-square-o',
                            'title' => 'Editar'
                        ),
                        'visible' => '$data->status ==  "0" AND ($data->tutor == Yii::app()->user->getState("id"))'
                    ),
                    'pdf' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl($data->proposal)',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-download',
                            'target' => '_blank',
                            'title' => 'Descargar Propuesta',
                            'download' => true
                        ),
                        'visible' => '$data->status >  "3"'
                    ),
                    'manuscript' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl($data->manuscript)',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-cloud-download',
                            'target' => '_blank',
                            'title' => 'Descargar Proyecto',
                            'download' => true
                        ),
                        'visible' => '$data->status ==  "13"'
                    )
                ),
                'htmlOptions' => array(
                    'style' => 'width: 50px;'
                )
            )
        )
    ));
?>