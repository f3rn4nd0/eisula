<?php
/* @var $this UserController */
/* @var $model UserModel */


?>


<h1>Administrar Usuarios</h1>



<?php
$this->widget('zii.widgets.grid.CGridView', 
    array(
        'id' => 'user-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'dni',
            'name',
            'last_name',
            'email',
            array(
                'name' => 'department',
                'value' => '$data->get_department()',
                'filter' => CHtml::activeDropDownList($model, 'department', UserModel::$list_department, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'name' => 'type',
                'value' => '$data->get_type()',
                'filter' => CHtml::activeDropDownList($model, 'type', UserModel::$list_type, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'class' => 'CButtonColumn',
                'viewButtonUrl' => 'Yii::app()->controller->createAbsoluteUrl("user/view/{$data->id}")',
                'updateButtonUrl' => 'Yii::app()->controller->createAbsoluteUrl("user/Update/{$data->id}")',
                'deleteButtonUrl' => 'Yii::app()->controller->createAbsoluteUrl("user/delete/{$data->id}")',
                'template' => '{view} {update} {delete} {curriculum}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-search',
                            'title' => 'Detalles'
                        ),
                        'visible' => ' true'
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil-square-o',
                            'title' => 'Editar'
                        ),
                        'visible' => ' true'
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-times',
                            'title' => 'Eliminar'
                        ),
                        'visible' => ' true'
                    ),
                    'curriculum' => array(
                        'label' => '',
                        'url' => 'Yii::app()->createAbsoluteUrl($data->teacher->curriculum)',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-file-pdf-o',
                            'target' => '_blank',
                            'title' => 'Descargar Curriculum',
                            'download' => true
                        ),
                        'click' => '...',
                        'visible' => '($data->type ==  "2" OR $data->type == "3") AND $data->teacher AND $data->teacher->curriculum'
                    )
                )
            )
        )
    ));
?>
