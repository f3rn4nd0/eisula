<?php
/* @var $this SiteController */
/* @var $model Recovery */
/* @var $form CActiveForm  */
 
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login'
);
?>

<h2 class="form-signin-heading">Por favor Introduzca Su Usuario y
	Contraseña</h2>


<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'login-form',
        'focus' => array($model,'username'),
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<p class="note">
	Los Campos <span class="required">*</span> son requeridos.
</p>

<div class="form-group">
            <?php echo $form->labelEx($model,'username', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->textField($model,'username', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>
</div>



<div class="form-group">
                <?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->passwordField($model,'password', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'password'); ?>
	        </div>
</div>

<div class="form-group">
            <div class="col-lg-2">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('site/recovery');?>"> 
                Olvide mi Contraseña</a>
             </div>

</div>
<div class="form-group buttons">
        <?php echo CHtml::submitButton('Acceder',array('class'=>'btn btn-medium btn-primary')); ?>
        <?php $this->endWidget(); ?>
</div>

<!-- form -->