<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Carta de Compromiso</title>
<style type="text/css">
.estilo1 {
	width: 1000px;
	height: auto;
}

span {
	width: 350px;
}
</style>
</head>
<body>
	<div class="form">


	<div align="center">
			<img alt="logo"
				src="<?php echo Yii::app()->baseUrl . "/resources/img/ing_logo.png"; ?>">
			<br></br>
		</div>
		<div align="center">
			<h1>PROYECTO DE GRADO</h1>
			<h2>CARTA DE COMPROMISO</h2>
			<br></br>
		</div>
			<div align="justify" class="estilo1">



				<p>
					Quien suscribe, Prof <?php echo $form->dropDownList($model, 'name', 
                    CHtml::listData(User::model()->findAllByAttributes(array('type' => '2')), 'id', 'name'), 
					    array('class' => 'form-control'));?>, titular de
					la cédula de identidad Nº
					<?php echo 'CEDULA000000'?>, mediante la presente me comprometo a
					ser el Profesor(a) Tutor(a) del Proyecto de grado del (de la ) Br.
					<?php echo 'NOMBRE DE LA SESION '?>, titular de la cédula de identidad
					Nº <?php echo 'CEDULA DE LA SESION '?>, el cual será desarrollado en el área
					temática
					<?php echo '"TITULO"'?>
					durante el semestre
					<label for="semester"></label> <select name="semester"
						id="semester">
						<option>A-2015</option>
						<option>B-2015</option>
						<option>A-2016</option>
						<option>B-2016</option>
						<option>A-2017</option>
						<option>B-2017</option>
						<option>A-2018</option>
						<option>B-2018</option>
					</select> , mismo que se regirá de acuerdo al Reglamento de
					Proyectos de Grado de la Escuela de Ingeniería de Sistemas vigente.
					<br> <br> <br> Si el Proyecto de Grado será cursado con una última materia,
				indique el nombre de la misma
				<?php echo 'MATERIA '?>
				</p>

			</div>
			<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>




			<div style="text-indent: 250pt;" align="left">


				<p>Bachiller</p>


			</div>
			<div style="text-indent: 10pt;" align="right">
			</div>

				<p>Profesor</p>
		
		<br>
		<div align="center">
			<p> <?php
print "Mérida " . date("d");
print " de " . date("F");
print " de " . date("Y")?> 
	           
	           </p>
		</div>

	</div>
</body>
</html>