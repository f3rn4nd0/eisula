<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
 
$this->pageTitle = Yii::app()->name . ' - restablecer Contraseña';
$this->breadcrumbs = array(
    'Restablecer Contraseña'
);

?>

<h2 class="form-signin-heading">Por favor Introduzca Su Nueva Contraseña</h2>


<?php

$form = $this->beginWidget('CActiveForm', 
    array(
        'method' => 'POST',
        'action' => Yii::app()->createUrl('site/password'),
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<p class="note">
	Los Campos <span class="required">*</span> son requeridos.
</p>
    
    <?php echo $form->hiddenField($model, 'dni'); ?>
    <?php echo $form->hiddenField($model, 'token'); ?>
    
    <div class="form-group">
                <?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->passwordField($model,'password', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'password'); ?>
	        </div>
	</div>
    
    <div class="form-group">
                <?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->passwordField($model,'password1', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'password1'); ?>
	        </div>
	</div>
   

	<div class="form-group buttons">
        <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-medium btn-primary')); ?>
    </div>

<?php $this->endWidget();?>
