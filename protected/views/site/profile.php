<?php
/* @var $this UserController */
/* @var $model   */
/* @var $form CActiveForm */



$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>
<br></br>
		<h1>Editar Perfil</h1>
		<note> Solo puede Editar <font color="#ff0000">CORREO</font> y/o <font color="#ff0000">CLAVE</font> de acceso.</note>
		<br></br>
		
<?php echo $form->errorSummary($model); ?>

<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
				<div class="form-control"><?php echo Yii::app()->user->getState('username')?></div>
		</div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <div class="form-control"><?php echo Yii::app()->user->getState('name'); ?></div>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'last_name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <div class="form-control"><?php echo Yii::app()->user->getState('last_name')?></div>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
        <div class="col-lg-3">
        		<div class="form-control"><?php echo $model->get_department();?></div>
        </div>
</div>


<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-3">
		      <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		      <?php echo $form->error($model,'email'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
        <div class="col-lg-3">
        		<?php echo $form->passwordField($model,'password',array('size'=>32,'maxlength'=>32, 'class'=> 'form-control')); ?>
        		<?php echo $form->error($model,'password'); ?>
        </div>
</div>



<div class="form-group buttons">
		<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-medium btn-primary')); ?> 
</div>
<?php $this->endWidget(); ?>


<!-- form -->