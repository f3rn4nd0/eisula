<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
 
$this->pageTitle = Yii::app()->name . ' - recuperar Contraseña';
$this->breadcrumbs = array(
    'Recuperar Contraseña'
);

?>

<h2 class="form-signin-heading">Por favor Introduzca Su Usuario y Correo </h2>


<?php

$form = $this->beginWidget('CActiveForm', 
    array(
        'method' => 'POST',
        'action' => Yii::app()->createUrl('site/recovery'),
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>

<p class="note">
	Los Campos <span class="required">*</span> son requeridos.
</p>
    <div class="form-group">
            <?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->textField($model,'dni', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'dni'); ?>
            </div>
	</div>
    
    <div class="form-group">
            <?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
            <div class="col-lg-2">
                <?php echo $form->textField($model,'email', array('class'=>"form-control")); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
	</div>

	<div class="form-group buttons">
        <?php echo CHtml::submitButton('Enviar',array('class'=>'btn btn-medium btn-primary')); ?>
    </div>

<?php $this->endWidget();?>
