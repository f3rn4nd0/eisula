<?php
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = array(
    'Proyectos' => array(
        'index'
    ),
    'Administrar'
);
array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'prioryty',
        array(
            'name' => 'department',
            'value' => $model->get_department()
        )
    )
);
?>

<h1>Proyecto:  <?php echo $model->title; ?></h1>
<div class="form">
<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'check-form',
        'focus' => array(
            $model,
            'name'
        ),
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>
    
<?php
$this->widget('zii.widgets.CDetailView', 
    array(
        'data' => $model,
        'attributes' => array(
            //'id',
            'title',
            'priority',
            'semester',
            array(
                'name' => 'department',
                'value' => $model->get_department()
            ),
            array(
                'name' => 'student_id',
                'value' => $model->get_name($model->student_id)
            ),
            array(
                'name' => 'status',
                'type' => 'raw',
                'value' => $form->dropDownList($model, 'status', 
                    array(
                        '1' => 'Aceptar',
                        '2' => 'Rechazar'
                    ), 
                    array(
                        'class' => 'col-lg-3',
                        'prompt' => 'Seleccione'
                    ))
            )
        )
    ));
?>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-medium btn-primary'));?>
		      
	</div>

</div>

<?php $this->endWidget(); ?>

