<?php
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name;
$this->breadcrumbs = array(
    'Proyectos' => array(
        'index'
    ),
    'Administrar'
);
array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'prioryty',
        array(
            'name' => 'department',
            'value' => $model->get_department()
        )
    )
);
?>

<h1>Proyecto:   <?php echo $model->title; ?></h1>
<div class="form">
<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'check-form',
        'focus' => array(
            $model,
            'name'
        ),
        'clientOptions' => array(
            'validateOnSubmit' => true
        ),
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>
    
<?php
$this->widget('zii.widgets.CDetailView', 
    array(
        'data' => $model,
        'attributes' => array(
            'title',
            'priority',
            'semester',
            array(
                'name' => 'department',
                'value' => $model->get_department()
            ),
            array(
                'name' => 'student_id',
                'value' => $model->get_name($model->student_id)
            ),
            array(
                'name' => 'tutor',
                'value' => $model->get_name($model->tutor)
            ),
            array(
                'name' => 'cotutor',
                'value' => $model->get_name($model->cotutor)
            ),
            array(
                'name' => 'jury1',
                'value' => $model->get_name($model->jury1)
            ),
            array(
                'name' => 'jury2',
                'value' => $model->get_name($model->jury2)
            ),
            array(
                'name' => 'jury3',
                'value' => $model->get_name($model->jury3)
            ),
            array(
                'name' => 'date_present_advance',
                'value' => preg_replace('/\//', ' de ', 
                    Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $model->date_present_advance))
            ),
            array(
                'name' => 'porcentage'
            ),
            array(
                'name' => 'observations'
            ),
            array(
                'name' => 'score_final'
            ),
            array(
                'name' => 'date_present',
                'value' => preg_replace('/\//', ' de ', 
                    Yii::app()->dateFormatter->format('dd/MMMM/yyyy', $model->date_present))
            ),
            array(
                'name' => 'status',
                'value' => $model->get_status($model->student_id)
            )
        )
    ));
?>


</div>

<?php $this->endWidget(); ?>

