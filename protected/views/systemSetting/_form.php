<?php
/* @var $this SystemSettingController */
/* @var $model SystemSetting */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'system-setting-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jefe_depto_control',  array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
		          <?php
            echo $form->dropDownList($model, 'jefe_depto_control', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="1" AND type="2"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                ));
            ?>
		          <?php echo $form->error($model,'jefe_depto_control'); ?>
		      </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'secrt_depto_control', array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
		          <?php
            echo $form->dropDownList($model, 'secrt_depto_control', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="1" AND type="3"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                ));
            ?>
		          <?php echo $form->error($model,'secrt_depto_control'); ?>
		      </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jefe_depto_computacion', array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
		          <?php
            echo $form->dropDownList($model, 'jefe_depto_computacion', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="3" AND type="2"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                ));
            ?>
		          <?php echo $form->error($model,'jefe_depto_computacion'); ?>
		      </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'secrt_depto_computacion', array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
    		      <?php
            echo $form->dropDownList($model, 'secrt_depto_computacion', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="3" AND type="3"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                ));
            ?>
	       	       <?php echo $form->error($model,'secrt_depto_computacion'); ?>
	          </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jefe_depto_investigacion', array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
		          <?php
            echo $form->dropDownList($model, 'jefe_depto_investigacion', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="2" AND type="2"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                ));
            ?>
		          <?php echo $form->error($model,'jefe_depto_investigacion'); ?>
		      </div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'secrt_depto_investigacion', array('class'=>"col-lg-2 control-label")); ?>
		      <div class="col-lg-4">
		          <?php
            echo $form->dropDownList($model, 'secrt_depto_investigacion', 
                CHtml::listData(
                    User::model()->findAll(
                        array(
                            'condition' => 'department="2" AND type="3"',
                            'select' => 't.id, CONCAT(t.name, " ", t.last_name) as name'
                        )), 'id', 'name'), array(
                    'prompt' => 'Seleccione',
                    'class' => 'form-control'
                    )
                );
            ?>
		          <?php echo $form->error($model,'secrt_depto_investigacion'); ?>
		      </div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'semester', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		 		<?php echo $form->textField($model,'semester', array('class'=>'form-control')); ?>
		 		<?php echo $form->error($model,'semester'); ?>
		 </div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'next_semester', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		 		<?php echo $form->textField($model,'next_semester', array('class'=>'form-control')); ?>
		 		<?php echo $form->error($model,'next_semester'); ?>
		 </div>
	</div>


	<div class="form-group buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', array('class'=>'btn btn-medium btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<!-- form -->