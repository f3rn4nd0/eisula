<?php
/* @var $this SystemSettingController */
/* @var $model SystemSetting */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jefe_depto_control'); ?>
		<?php echo $form->textField($model,'jefe_depto_control'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jefe_depto_computacion'); ?>
		<?php echo $form->textField($model,'jefe_depto_computacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jefe_depto_investigacion'); ?>
		<?php echo $form->textField($model,'jefe_depto_investigacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'secrt_depto_control'); ?>
		<?php echo $form->textField($model,'secrt_depto_control'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'secrt_depto_investigacion'); ?>
		<?php echo $form->textField($model,'secrt_depto_investigacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'secrt_depto_computacion'); ?>
		<?php echo $form->textField($model,'secrt_depto_computacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->