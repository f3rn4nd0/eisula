<?php
/* @var $this SystemSettingController */
/* @var $data SystemSetting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jefe_depto_control')); ?>:</b>
	<?php echo CHtml::encode($data->jefe_depto_control); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jefe_depto_computacion')); ?>:</b>
	<?php echo CHtml::encode($data->jefe_depto_computacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jefe_depto_investigacion')); ?>:</b>
	<?php echo CHtml::encode($data->jefe_depto_investigacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secrt_depto_control')); ?>:</b>
	<?php echo CHtml::encode($data->secrt_depto_control); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secrt_depto_investigacion')); ?>:</b>
	<?php echo CHtml::encode($data->secrt_depto_investigacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secrt_depto_computacion')); ?>:</b>
	<?php echo CHtml::encode($data->secrt_depto_computacion); ?>
	<br />


</div>