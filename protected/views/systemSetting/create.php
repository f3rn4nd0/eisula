<?php
/* @var $this SystemSettingController */
/* @var $model SystemSetting */

$this->breadcrumbs=array(
	'System Settings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SystemSetting', 'url'=>array('index')),
	array('label'=>'Manage SystemSetting', 'url'=>array('admin')),
);
?>

<h1>Create SystemSetting</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>