<?php
/* @var $this SystemSettingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'System Settings',
);

$this->menu=array(
	array('label'=>'Create SystemSetting', 'url'=>array('create')),
	array('label'=>'Manage SystemSetting', 'url'=>array('admin')),
);
?>

<h1>System Settings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
