<?php
/* @var $this SystemSettingController */
/* @var $model SystemSetting */

$this->breadcrumbs=array(
	'System Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SystemSetting', 'url'=>array('index')),
	array('label'=>'Create SystemSetting', 'url'=>array('create')),
	array('label'=>'Update SystemSetting', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SystemSetting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SystemSetting', 'url'=>array('admin')),
);
?>

<h1>View SystemSetting #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jefe_depto_control',
		'jefe_depto_computacion',
		'jefe_depto_investigacion',
		'secrt_depto_control',
		'secrt_depto_investigacion',
		'secrt_depto_computacion',
	),
)); ?>
