<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */

$form = $this->beginWidget('CActiveForm', 
    array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => "form-horizontal"
        )
    ));
?>



<p class="note">
	Los campos con <span class="required">*</span> son requeridos.
</p>

<?php echo $form->errorSummary($model); ?>

<div class="form-group">
		<?php echo $form->labelEx($model,'dni', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
				<?php echo $form->textField($model,'dni',array('size'=>10,'maxlength'=>10, 'class'=> 'form-control')); ?>
				<?php echo $form->error($model,'dni'); ?>
		</div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		      <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		      <?php echo $form->error($model,'name'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'last_name', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		      <?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		      <?php echo $form->error($model,'last_name'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'email', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		      <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		      <?php echo $form->error($model,'email'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('class'=>"col-lg-2 control-label")); ?>
        <div class="col-lg-4">
        		<?php echo $form->passwordField($model,'password',array('size'=>32,'maxlength'=>32, 'class'=> 'form-control')); ?>
        		<?php echo $form->error($model,'password'); ?>
        </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'department', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		      <?php echo $form->dropDownList($model, 'department', UserModel::$list_department, array('class'=>'form-control', 'prompt' => 'Selecione una Opción')); ?>
		      <?php echo $form->error($model,'department'); ?>
	   </div>
</div>

<div class="form-group">
		<?php echo $form->labelEx($model,'type', array('class'=>"col-lg-2 control-label")); ?>
		<div class="col-lg-4">
		      <?php
        
echo $form->dropDownList($model, 'type', 
            array(
                '0' => 'Administrador',
                '1' => 'Estudiante',
                '2' => 'Profesor',
                '3' => 'Personal Administrativo',
                '4' => 'Dirección de la Escuela'
            ), array(
                'class' => 'form-control',
                'prompt' => 'Selecione una Opción'
            ));
        ?>
		      <?php echo $form->error($model,'type'); ?>
	   </div>
</div>

<div class="form-group buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',array('class'=>'btn btn-medium btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>


<!-- form -->