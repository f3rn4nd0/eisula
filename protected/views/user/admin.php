<?php
/* @var $this UserController */
/* @var $model UserModel */

?>
<h1>Administrar Usuarios</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', 
    array(
        'id' => 'user-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'dni',
            'name',
            'last_name',
            'email',
            array(
                'name' => 'department',
                'value' => '$data->get_department()',
                'filter' => CHtml::activeDropDownList($model, 'department', UserModel::$list_department, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'name' => 'type',
                'value' => '$data->get_type()',
                'filter' => CHtml::activeDropDownList($model, 'type', UserModel::$list_type, 
                    array(
                        'prompt' => ''
                    ))
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-search',
                            'title' => 'Detalles'
                        ),
                        'visible' => ' true'
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-pencil-square-o',
                            'title' => 'Editar'
                        ),
                        'visible' => ' true'
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array(
                            'class' => 'fa fa-times',
                            'title' => 'Eliminar'
                        ),
                        'visible' => ' true'
                    )
                )
            )
        )
    ))?>
