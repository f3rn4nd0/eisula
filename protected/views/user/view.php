<?php
/* @var $this UserController */
/* @var $model UserModel */
?>

<h1>Ver Usuarios</h1>

<?php
$this->widget ( 'zii.widgets.CDetailView', array (
		'data' => $model,
		'attributes' => array(
		'dni',
				'name',
				'last_name',
				'email',
		array (
						'name' => 'department',
						'value' => $model->get_department() 
				),
				array (
						'name' => 'type',
						'value' => $model->get_type()
				)
             
		)
		 
) );
?>
